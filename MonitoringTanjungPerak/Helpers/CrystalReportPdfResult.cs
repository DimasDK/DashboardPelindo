﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace MonitoringTanjungPerak.Helpers
{
    public class CrystalReportPdfResult : ActionResult
    {
        //public static void Export<T>(List<T> entities,string reportName)
        //{

        //    ReportDocument rd = new ReportDocument();
        //    rd.Load(Path.Combine(HostingEnvironment.MapPath("~/Reports"), reportName));
        //    rd.SetDataSource(entities);

        //    HttpResponse response = HttpContext.Current.Response;
        //    response.Buffer = true;
        //    response.ClearContent();
        //    response.ClearHeaders();
        //    response.ContentType = "application/pdf";
        //    try
        //    {
        //        Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //        stream.Seek(0, SeekOrigin.Begin);
        //    }
        //    catch (Exception e)
        //    {

        //        throw;
        //    }
        //    //    System.IO.Path.GetFullPath(HttpServerUtility("~/App_Data/Images/no-image.png")));
        //}
        private readonly byte[] _contentBytes;

        //public CrystalReportPdfResult(string reportPath, object dataSet)
        //{
        //    ReportDocument reportDocument = new ReportDocument();
        //    reportDocument.Load(reportPath + ".rpt");
        //    reportDocument.SetDataSource(dataSet);
        //    _contentBytes = StreamToBytes(reportDocument.ExportToStream(ExportFormatType.PortableDocFormat));
        //}
        private static byte[] StreamToBytes(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.ApplicationInstance.Response;
            response.Clear();
            response.Buffer = false;
            response.ClearContent();
            response.ClearHeaders();
            response.Cache.SetCacheability(HttpCacheability.Public);
            response.ContentType = "application/pdf";

            using (var stream = new MemoryStream(_contentBytes))
            {
                stream.WriteTo(response.OutputStream);
                stream.Flush();
            }
        }
    }
}