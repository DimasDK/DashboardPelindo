﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Oracle.ManagedDataAccess.Client;

namespace Oracle.ManagedDataAccess.Client
{
    public static class OracleHelper
    {

        public static void AddArray<T>(this OracleParameterCollection parameters,
       string name,
       OracleDbType dbType,
       T[] array,
       ParameterDirection direction,
       T emptyArrayValue)
        {
            parameters.Add(new OracleParameter
            {
                ParameterName = name,
                OracleDbType = dbType,
                CollectionType = OracleCollectionType.PLSQLAssociativeArray
            });

            // oracle does not support passing null or empty arrays.
            // so pass an array with exactly one element
            // with a predefined value and use it to check
            // for empty array condition inside the proc code
            if (array == null || array.Length == 0)
            {
                parameters[name].Value = new T[1] { emptyArrayValue };
                parameters[name].Size = 1;
            }
            else
            {
                parameters[name].Value = array;
                parameters[name].Size = array.Length;
            }
        }
    }
}