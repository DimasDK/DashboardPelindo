﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;

namespace MonitoringTanjungPerak.Helpers
{
    public static class Conversion
    {
        public static string GetIP4Address()
        {
            string IP4Address = String.Empty;


            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }
            return IP4Address;
        }
        public static string MapPathReverse(string fullServerPath)
        {
            if (string.IsNullOrEmpty(fullServerPath))
            {
                return null;
            }
            string physicalApplicationPath = HttpContext.Current.Request.PhysicalApplicationPath;

            if (string.IsNullOrEmpty(physicalApplicationPath))
            {
                return null;
            }

            return @"~/" + fullServerPath.Replace(physicalApplicationPath, String.Empty).Replace(@"\", "/");
        }

        public static string GetRelativePath(string absolutePath)
        {
            if (string.IsNullOrEmpty(absolutePath))
            {
                return null;
            }

            string physicalPath = HttpContext.Current.Request.MapPath(absolutePath);

            return MapPathReverse(physicalPath);
        }

        public static string ResolveUrl(string url)
        {
            Page page = HttpContext.Current.Handler as Page;

            if (page == null)
            {
                return url;
            }

            return (page).ResolveUrl(url);
        }

        public static short TryCastShort(object value)
        {
            short retVal = 0;

            if (value != null)
            {
                //string numberToParse = RemoveGroupping(value.ToString());
                string numberToParse = value.ToString();

                if (short.TryParse(numberToParse, out retVal))
                {
                    return retVal;
                }
            }

            return retVal;
        }

        public static long TryCastLong(object value)
        {
            long retVal = 0;

            if (value != null)
            {
                //string numberToParse = RemoveGroupping(value.ToString());
                string numberToParse = value.ToString();

                if (long.TryParse(numberToParse, out retVal))
                {
                    return retVal;
                }
            }

            return retVal;
        }

        public static float TryCastSingle(object value)
        {
            float retVal = 0;

            if (value != null)
            {
                //string numberToParse = RemoveGroupping(value.ToString());
                string numberToParse = value.ToString();

                if (float.TryParse(numberToParse, out retVal))
                {
                    return retVal;
                }
            }

            return retVal;
        }

        public static double TryCastDouble(object value)
        {
            double retVal = 0;

            if (value != null)
            {
                //string numberToParse = RemoveGroupping(value.ToString());
                string numberToParse = value.ToString();

                if (double.TryParse(numberToParse, out retVal))
                {
                    return retVal;
                }
            }

            return retVal;
        }

        public static int TryCastInteger(object value)
        {
            int retVal = 0;

            if (value != null)
            {
                if (value is bool)
                {
                    if (Convert.ToBoolean(value, CultureInfo.InvariantCulture))
                    {
                        return 1;
                    }
                }

                //string numberToParse = RemoveGroupping(value.ToString());
                string numberToParse = value.ToString();

                if (int.TryParse(numberToParse, out retVal))
                {
                    return retVal;
                }
            }

            return retVal;
        }

        public static DateTime TryCastDate(object value)
        {
            try
            {
                if (value == DBNull.Value)
                {
                    return DateTime.MinValue;
                }

                return Convert.ToDateTime(value, Thread.CurrentThread.CurrentCulture);
            }
            catch (FormatException)
            {
                //swallow the exception
            }
            catch (InvalidCastException)
            {
                //swallow the exception
            }

            return DateTime.MinValue;
        }

        public static decimal TryCastDecimal(object value)
        {
            decimal retVal = 0;

            if (value != null)
            {
                //string numberToParse = RemoveGroupping(value.ToString());
                string numberToParse = value.ToString();

                if (decimal.TryParse(numberToParse, out retVal))
                {
                    return retVal;
                }
            }

            return retVal;
        }
        public static decimal TryCastDecimalRound(object value)
        {
            decimal retVal = 0;

            if (value != null)
            {
                //string numberToParse = RemoveGroupping(value.ToString());
                string numberToParse = value.ToString();
                if (decimal.TryParse(numberToParse, out retVal))
                {
                    retVal = Math.Round(retVal, 0);
                    return retVal;
                }

            }

            return retVal;
        }

        public static bool TryCastBoolean(object value)
        {
            bool retVal = false;

            if (value != null)
            {
                if (value is string)
                {
                    if (value.ToString().ToLower(Thread.CurrentThread.CurrentCulture).Equals("yes"))
                    {
                        return true;
                    }

                    if (value.ToString().ToLower(Thread.CurrentThread.CurrentCulture).Equals("true"))
                    {
                        return true;
                    }
                }

                if (bool.TryParse(value.ToString(), out retVal))
                {
                    return retVal;
                }
            }

            return retVal;
        }

        public static bool IsNumeric(string value)
        {
            double number;
            return double.TryParse(value, out number);
        }

        public static string TryCastString(object value)
        {
            try
            {
                if (value != null)
                {
                    if (value is bool)
                    {
                        if (Convert.ToBoolean(value, CultureInfo.InvariantCulture))
                        {
                            return "true";
                        }

                        return "false";
                    }

                    if (value == DBNull.Value)
                    {
                        return string.Empty;
                    }

                    string retVal = value.ToString();
                    return retVal;
                }

                return string.Empty;
            }
            catch (FormatException)
            {
                //swallow the exception
            }
            catch (InvalidCastException)
            {
                //swallow the exception            
            }

            return string.Empty;
        }

        public static string HashSha512(string password, string salt)
        {
            if (password == null)
            {
                return null;
            }

            if (salt == null)
            {
                return null;
            }

            byte[] bytes = Encoding.Unicode.GetBytes(password + salt);
            using (SHA512CryptoServiceProvider hash = new SHA512CryptoServiceProvider())
            {
                byte[] inArray = hash.ComputeHash(bytes);
                return Convert.ToBase64String(inArray);
            }
        }


    }
}