﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using MonitoringTanjungPerak.DAL;
using MonitoringTanjungPerak.Entities;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Web.Security;
using MonitoringTanjungPerak.Models;

namespace MonitoringTanjungPerak.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            HttpCookie cookiess = Request.Cookies["autoReload"];
            if (cookiess == null)
            {
                try
                {
                    HttpCookie cookie = new HttpCookie("autoReload");
                    cookie.Values["homeIndexCounterVesselProgress"] = "15";
                    cookie.Values["homeIndexCounterGerakanKapal"] = "15";
                    cookie.Values["homeIndexCounterAvaPandu"] = "15";
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie);
                }
                catch (Exception e)
                {
                    string a = e.Message.ToString();
                }
            }

            return View();
        }

        public ActionResult PmhTambatHrIniByKapal(string mdmg, string nm, string cbg)
        {
            try
            {
                ViewBag.kodedmg = mdmg.ToString();
                ViewBag.namadmg = nm.ToString();
                ViewBag.kodecbg = cbg.ToString();
            }
            catch (Exception)
            { }
            return View();
        }

        public ActionResult GerakanKapalPandu(string movementType, string location, string locName)
        {
            try
            {
                ViewBag.mvType = movementType.ToLower();
                ViewBag.lcName = locName.ToString();
                ViewBag.lcCode = location.ToString();
            }
            catch (Exception)
            { }
            return View();
        }

        public ActionResult JadwalTambatanKapal(string movementType, string location, string locName, string cbg)
        {
            try
            {
                ViewBag.mvType = movementType.ToLower();
                ViewBag.lcName = locName.ToString();
                ViewBag.lcCode = location.ToString();
                ViewBag.kodecbg = cbg.ToString();
            }
            catch (Exception)
            { }
            return View();
        }

        public ActionResult JadwalKeberangkatanKapal(string movementType, string location, string locName, string cbg)
        {
            try
            {
                ViewBag.mvType = movementType.ToLower();
                ViewBag.lcName = locName.ToString();
                ViewBag.lcCode = location.ToString();
                ViewBag.kodecbg = cbg.ToString();
            }
            catch (Exception)
            { }
            return View();
        }

        public ActionResult VesselProgress(string movementType, string location, string locName)
        {
            try
            {
                ViewBag.mvType = movementType.ToLower();
                ViewBag.lcName = locName.ToString();
                ViewBag.lcCode = location.ToString();
            }
            catch(Exception)
            {}
            return View();
        }

        public ActionResult StatusPandu(string movementType, string location, string locName)
        {
            try
            {
                ViewBag.mvType = movementType.ToLower();
                ViewBag.lcName = locName.ToString();
                ViewBag.lcCode = location.ToString();
            }
            catch (Exception)
            { }
            return View();
        }

        public JsonResult CounterAllPmhTambat() {
            List<VW_COUNTER_PENETAPAN_TAMBAT> result = new List<VW_COUNTER_PENETAPAN_TAMBAT>();
            if (Request.IsAjaxRequest())
            {
                PermohonanTambat dal = new PermohonanTambat();
                result = dal.GetCounterPmhTambatByDermaga();
            }

            //result.Add(new VW_COUNTER_PENETAPAN_TAMBAT() { MDMG_KODE = "06", MDMG_NAMA = "JAMRUD UTARA", COUNTER_TODAY = 200, COUNTER_YESTERDAY = 10 });
            //result.Add(new VW_COUNTER_PENETAPAN_TAMBAT() { MDMG_KODE = "06", MDMG_NAMA = "JAMRUD SELATAN", COUNTER_TODAY = 300, COUNTER_YESTERDAY = 0 });
            //result.Add(new VW_COUNTER_PENETAPAN_TAMBAT() { MDMG_KODE = "06", MDMG_NAMA = "REDE", COUNTER_TODAY = 80, COUNTER_YESTERDAY = 5 });
            //result.Add(new VW_COUNTER_PENETAPAN_TAMBAT() { MDMG_KODE = "06", MDMG_NAMA = "JAMUANG / AMBANG LUAR", COUNTER_TODAY = 100, COUNTER_YESTERDAY = 2 });

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CounterAllPmhGrkKapal()
        {
            List<VW_COUNTER_GERAK_KAPAL> result = new List<VW_COUNTER_GERAK_KAPAL>();
            if (Request.IsAjaxRequest())
            {
                PermohonanGerak dal = new PermohonanGerak();
                result = dal.GetCounterGerakKapal();
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult SetCookiesReload()
        {
            string result = "";
            HttpCookie cookie = Request.Cookies["autoReload"];
            if (Request.IsAjaxRequest())
            {
                var queryString = Request.QueryString;
                try
                {
                    string pv = queryString["reloadParamValue"].ToString();
                    string cn = queryString["reloadParamName"].ToString();

                    cookie.Values[cn] = pv;
                    Response.SetCookie(cookie);
                }
                catch (Exception e)
                {
                    cookie.Values["errorException"] = e.Message.ToString();
                    Response.SetCookie(cookie);
                }
            }
            cookie.Expires = DateTime.UtcNow.AddDays(30);
            Response.Cookies.Add(cookie);

            result = cookie.ToString();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult SetDetailCookies() {
            string result = "";

            HttpCookie cookie = Request.Cookies["CookieLastDetailVesselProgress"];
            if (Request.IsAjaxRequest())
            {
                var queryString = Request.QueryString;
                string rp = queryString["tppkb1Nomor"].ToString();
                if (cookie != null)
                {
                    string tppkb1nomor = cookie.Values["tppkb1Nomor"].ToString();
                    if (tppkb1nomor != rp)
                    {
                        cookie.Values["tppkb1Nomor"] = rp;
                    }
                }
                else
                {
                    cookie = new HttpCookie("CookieLastDetailVesselProgress");
                    cookie.Values["tppkb1Nomor"] = rp;
                }
            }
            cookie.Expires = DateTime.UtcNow.AddDays(30);
            Response.Cookies.Add(cookie);

            result = cookie.Values["tppkb1Nomor"].ToString();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPermohonanTambatHarini()
        {
            PermohonanTambat.DataListVesselProgres result = new PermohonanTambat.DataListVesselProgres();

            if (Request.IsAjaxRequest())
            {
                try
                {
                    var queryString = Request.QueryString;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search"];
                    string fLine = queryString["fLine"];

                    string mdmgKode = queryString["mdmgKode"];
                    string cbgKode = queryString["kodecbg"];

                    PermohonanTambat dal = new PermohonanTambat();
                    result = dal.GetDataPermohonanTambatHarIniByDermaga(draw, start, length, search, mdmgKode, fLine, cbgKode);
                }
                catch (Exception e)
                {
                    result.mesaage = e.Message.ToString();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMonitoringTambatHarini()
        {
            PermohonanTambat.DataListVesselTambat result = new PermohonanTambat.DataListVesselTambat();

            if (Request.IsAjaxRequest())
            {
                try
                {
                    var queryString = Request.QueryString;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search"];
                    string fLine = queryString["fLine"];

                    string mdmgKode = queryString["mdmgKode"];
                    string cbgKode = queryString["kodecbg"];

                    PermohonanTambat dal = new PermohonanTambat();
                    result = dal.GetDataPermohonanTambatHarIni(draw, start, length, search, fLine, cbgKode, mdmgKode);
                }
                catch (Exception e)
                {
                    result.mesaage = e.Message.ToString();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMonitoringKeberangkatanKapal()
        {
            PermohonanTambat.DataListVesselTambat result = new PermohonanTambat.DataListVesselTambat();

            if (Request.IsAjaxRequest())
            {
                try
                {
                    var queryString = Request.QueryString;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search"];
                    string fLine = queryString["fLine"];

                    string mdmgKode = queryString["mdmgKode"];
                    string cbgKode = queryString["kodecbg"];

                    PermohonanTambat dal = new PermohonanTambat();
                    result = dal.GetDataKeberangkatanKapal(draw, start, length, search, fLine, cbgKode, mdmgKode);
                }
                catch (Exception e)
                {
                    result.mesaage = e.Message.ToString();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPermohonanAntrianTambatHarini()
        {
            PermohonanTambat.DataListVesselProgres result = new PermohonanTambat.DataListVesselProgres();

            if (Request.IsAjaxRequest())
            {
                try
                {
                    var queryString = Request.QueryString;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search"];
                    string fLine = queryString["fLine"];

                    string mdmgKode = queryString["mdmgKode"];
                    string cbgKode = queryString["kodecbg"];

                    PermohonanTambat dal = new PermohonanTambat();
                    result = dal.GetDataPermohonanAntrianTambatByDermaga(draw, start, length, search, mdmgKode, fLine, cbgKode);
                }
                catch (Exception e)
                {
                    result.mesaage = e.Message.ToString();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult getDataDetailsKegiatanKapal()
        {
            BalanceKapalModel result = new BalanceKapalModel();
            if (Request.IsAjaxRequest())
            {
                var queryString = Request.QueryString;
                HttpCookie cookie = Request.Cookies["CookieLastDetailVesselProgress"];
                string tppkb1nomor = cookie.Values["tppkb1Nomor"].ToString();
                string mdmgKode = queryString["mdmgKode"];
                string cbgKode = queryString["kodecbg"];

                RealisasiBongkarMuat dal = new RealisasiBongkarMuat();
                PermohonanTambat dal2 = new PermohonanTambat();
                result.dataBalance = dal.GetDataBalanceBM(cbgKode, tppkb1nomor);
                result.dataKapal = dal2.GetDataTambatByKapal(mdmgKode, cbgKode, tppkb1nomor);                
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getDataMoreDetailKapal() {
            List<VW_MONITORING> result = new List<VW_MONITORING>();
            if (Request.IsAjaxRequest())
            {
                try
                {
                    HttpCookie cookie = Request.Cookies["CookieLastDetailVesselProgress"];
                    string tppkb1nomor = cookie.Values["tppkb1Nomor"].ToString();
                    
                    //var queryString = Request.QueryString;
                    //string tppkb1nomor = queryString["tppkb1nomor"].ToString();
                    RealisasiBongkarMuat dal = new RealisasiBongkarMuat();
                    result = dal.getDataMoreDetailsKapal(tppkb1nomor);
                }
                catch (Exception e)
                {
                    string rm = e.Message.ToString();
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult getDataGerakanKapal() {
            PermohonanGerak.DataListGerakanKapal result = new PermohonanGerak.DataListGerakanKapal();
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var queryString = Request.QueryString;
                    int draw = int.Parse(queryString["draw"]);
                    int start = int.Parse(queryString["start"]);
                    int length = int.Parse(queryString["length"]);
                    string search = queryString["search"];

                    string MovType = queryString["MovType"].ToString().ToUpper();
                    string locCode = queryString["locCode"].ToString().ToUpper();

                    PermohonanGerak dal = new PermohonanGerak();
                    result = dal.GetDataGerakanKapal(draw, start, length, search, MovType, locCode);
                }
                catch (Exception e)
                {
                    result.mesaage = e.Message.ToString();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CounterAvalibilitasPandu()
        {
            List<VW_COUNTER_PANDU> result = new List<VW_COUNTER_PANDU>();
            if (Request.IsAjaxRequest())
            {
                PanduDAL dal = new PanduDAL();
                result = dal.GetCounterPandu();
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AvalibilitasPandu(string movementType, string location, string locName)
        {
            if (movementType.ToUpper() == "STANDBY")
            {
                List<VW_PTG_PANDU_STANDBY_BGS> result = new List<VW_PTG_PANDU_STANDBY_BGS>();
                if (Request.IsAjaxRequest())
                {
                    PanduDAL dal = new PanduDAL();
                    result = dal.GetDataPanduStandby(location, locName);
                }
                return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
            }
            else if (movementType.ToUpper() == "ON_DUTY")
            {
                List<VW_PTG_PANDU_ONDUTY_BGS> result = new List<VW_PTG_PANDU_ONDUTY_BGS>();
                if (Request.IsAjaxRequest())
                {
                    PanduDAL dal = new PanduDAL();
                    result = dal.GetDataPanduOnduty(location, locName);
                }
                return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
            }
            else if (movementType.ToUpper() == "ON_BOARD")
            {
                List<VW_PTG_PANDU_ONBOARD_BGS> result = new List<VW_PTG_PANDU_ONBOARD_BGS>();
                if (Request.IsAjaxRequest())
                {
                    PanduDAL dal = new PanduDAL();
                    result = dal.GetDataPanduOnboard(location, locName);
                }
                return Json(new { data = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return null;
            }
        }
    }
}