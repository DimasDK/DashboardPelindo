﻿using System.Collections.Generic;
using System.Security.Claims;
using MonitoringTanjungPerak.DAL;
using System.Web.Mvc;
using MonitoringTanjungPerak.ViewHelper;

namespace MonitoringTanjungPerak.Controllers
{
    public class ViewHelperController : Controller
    {

        public JsonResult getTerminal(string p, string kdCabang)
        {
            var xKD_CABANG = kdCabang;
            IEnumerable<VW_TerminalModel> result = null;
            ViewHelperDAL dal = new ViewHelperDAL();
            result = dal.GetDataTerminal(xKD_CABANG, p);
            return Json(new { items = result, JsonRequestBehavior.AllowGet}, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getTerminalByCabang(string xKD_CABANG)
        {
            IEnumerable<VW_TerminalModel> result = null;
            ViewHelperDAL dal = new ViewHelperDAL();
            result = dal.GetDataTerminalByCabang(xKD_CABANG);
            return Json(new { items = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getPelabuhan(string p)
        {
            IEnumerable<VW_PelabuhanModel> result = null;
            ViewHelperDAL dal = new ViewHelperDAL();
            result = dal.GetDataPelabuhan(p);
            return Json(new { items = result, JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCabang()
        {
            IEnumerable<VW_Cabang> result = null;
            ViewHelperDAL dal = new ViewHelperDAL();
            result = dal.GetDataCabang();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}