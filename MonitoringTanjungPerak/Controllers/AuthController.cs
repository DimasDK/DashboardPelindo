﻿using System;
using System.Collections.Generic;
using MonitoringTanjungPerak.DAL;
using MonitoringTanjungPerak.Entities;
using MonitoringTanjungPerak.Models;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Dapper;

namespace MonitoringTanjungPerak.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {
        [Flags]
        public enum Permissions
        {
            View = (1 << 0),
            Add = (1 << 1),
            Edit = (1 << 2),
            Delete = (1 << 3),
            Admin = (View | Add | Edit | Delete)
        }
        
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {


            Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Expires", "0");

            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.ReturnUrl = returnUrl;
                var identity = new ClaimsIdentity(new[] {
                        new Claim(ClaimTypes.Name, "Abc"),
                        new Claim("USER_ID", "1239"),
                        new Claim("KD_CABANG", "2"),
                        new Claim("KD_CABANG_ORIG", "2"),
                        new Claim("ROLE_ID", "1239"),
                        new Claim("NAMA_CABANG", "Jamrud")
                    }, "ApplicationCookie");

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;

                authManager.SignIn(identity);

                return Redirect(GetRedirectUrl(returnUrl));

                //return View();
            }
            //return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            try
            {
                //AuthDAL dal = new AuthDAL();

                //APP_USER userInformation = dal.GetUserInformation(model.NIPP, model.Password);
                //APP_USER userInformation = dal.GetUserInformation("170102006", "pelindo3");
                //List<VW_MENU_ACTIVITIES> userActivities = dal.GetUserActivities(userInformation.USER_ROLE_ID);

                var identity = new ClaimsIdentity(new[] {
                        new Claim(ClaimTypes.Name, "Abc"),
                        new Claim("USER_ID", "1239"),
                        new Claim("KD_CABANG", "2"),
                        new Claim("KD_CABANG_ORIG", "2"),
                        new Claim("ROLE_ID", "1239"),
                        new Claim("NAMA_CABANG", "Jamrud")
                    }, "ApplicationCookie");

                
                //if (userInformation != null)
                //{
                //    var identity = new ClaimsIdentity(new[] {
                //    new Claim(ClaimTypes.Name, userInformation.USER_NAMA),
                //    new Claim("USER_ID", userInformation.ID.ToString()),
                //    new Claim("KD_CABANG", userInformation.KD_CABANG),
                //    new Claim("KD_CABANG_ORIG", userInformation.KD_CABANG),
                //    new Claim("ROLE_ID", userInformation.USER_ROLE_ID),
                //    new Claim("NAMA_CABANG", userInformation.NAMA_CABANG)
                //}, "ApplicationCookie");



                //if (userActivities.Count > 0)
                //    {
                //        foreach (var item in userActivities)
                //        {
                //            if (item.PERMISSION_ADD.Equals("1"))
                //                identity.AddClaim(new Claim(ClaimTypes.Role, item.NAMA_ACTIVITY + ".Add"));
                //            if (item.PERMISSION_DEL.Equals("1"))
                //                identity.AddClaim(new Claim(ClaimTypes.Role, item.NAMA_ACTIVITY + ".Delete"));
                //            if (item.PERMISSION_UPD.Equals("1"))
                //                identity.AddClaim(new Claim(ClaimTypes.Role, item.NAMA_ACTIVITY + ".Edit"));
                //            if (item.PERMISSION_VIW.Equals("1"))
                //                identity.AddClaim(new Claim(ClaimTypes.Role, item.NAMA_ACTIVITY + ".View"));
                //        }
                //    }

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;

                authManager.SignIn(identity);

                return Redirect(GetRedirectUrl(returnUrl));
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return View();
        }

        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.Action("Index", "Home");
            }

            return returnUrl;
        }

        public ActionResult Logout()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("Index", "Home");
        }
    }
}