﻿namespace MonitoringTanjungPerak.Entities
{
    public class VW_MONITORING
    {
        public string TPPKB1_NOMOR { get; set; }
        public string TPPKB1_NO_FORM { get; set; }
        public string PMH_JNS_JASA { get; set; }
        public string RCN_MULAI { get; set; }
        public string RCN_SELESAI { get; set; }
        public string TGL_MULAI { get; set; }
        public string TGL_SELESAI { get; set; }

        public string LOK_AWAL { get; set; }
        public string LOK_AKHIR { get; set; }
        public string DMG_AWAL { get; set; }
        public string DMG_AKHIR { get; set; }
        public string REC_STAT { get; set; }
        public string PROGRES { get; set; }

        public string KDAWAL { get; set; }
        public string KDAKHIR { get; set; }
    }
}