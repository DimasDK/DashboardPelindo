﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitoringTanjungPerak.Entities
{
    public class APP_USER
    {
        public string ID { get; set; }
        public string USER_LOGIN { get; set; }
        public string USER_PASSWORD { get; set; }
        public string USER_ROLE_ID { get; set; }
        public string KD_CABANG { get; set; }
        public string USER_NAMA { get; set; }
        public string NAMA_CABANG { get; set; }
    }
}