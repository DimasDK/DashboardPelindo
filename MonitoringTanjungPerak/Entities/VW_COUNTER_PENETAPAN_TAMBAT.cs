﻿namespace MonitoringTanjungPerak.Entities
{
    public class VW_COUNTER_PENETAPAN_TAMBAT
    {
        public string KD_CABANG { get; set; }
        public string NAMA_CABANG { get; set; }
        public string REA_KD_DMG { get; set; }
        public string REA_NAMA_DMG { get; set; }
        public string JML { get; set; }
        public string KELUAR { get; set; }
        public string MASUK { get; set; }
    }
}