﻿using System;
namespace MonitoringTanjungPerak.Entities
{
    public class VW_PENETAPAN_TAMBAT
    {
        public string TPPKB1_NOMOR { get; set; }
        public string TPPKB1_NO_FORM { get; set; }
        public string TPPKB1_NAMA_KAPAL { get; set; }
        public string AGEN { get; set; }
        public string PNT_MULAI { get; set; }
        public string PNT_SELESAI { get; set; }
        public string FIRST_LINE { get; set; }
        public string DERMAGA { get; set; }
        public string REC_STAT { get; set; }
        public string PROGRES { get; set; }
        public string KADE_MULAI { get; set; }
        public string KADE_AKHIR { get; set; }
    }

    public class VW_MON_VESPROG_BGS
    {
        public string KODE_CABANG { get; set; }
        public string ASAL_DATA { get; set; }
        public string NAMA_KAPAL { get; set; }
        public string KODE_KAPAL { get; set; }
        public string LOA { get; set; }
        public string GT_KAPAL { get; set; }
        public string NAMA_AGEN { get; set; }
        public string NAMA_PELABUHAN_ASAL { get; set; }
        public string NAMA_PELABUHAN_TUJUAN { get; set; }
        public string NO_PKK { get; set; }
        public string NO_PPK1 { get; set; }
        public string NO_PPK_JASA { get; set; }
        public DateTime? PNT_TGL_MULAI { get; set; }
        public DateTime? PNT_TGL_SELESAI { get; set; }
        public string PNT_KD_DMG { get; set; }
        public DateTime? REA_TGL_MULAI { get; set; }
        public DateTime? REA_TGL_SELESAI { get; set; }
        public string REA_KD_DMG { get; set; }
        public string REA_NAMA_DMG { get; set; }
        public string KADE_METER { get; set; }
        public string STATUS { get; set; }
        public string PROGRESS { get; set; }
    }

    public class VW_MON_VESTAMBAT_BGS
    {
        public string KODE_CABANG { get; set; }
        public string ASAL_DATA { get; set; }
        public string NAMA_KAPAL { get; set; }
        public string KODE_KAPAL { get; set; }
        public string LOA { get; set; }
        public string GT_KAPAL { get; set; }
        public string NAMA_AGEN { get; set; }
        public string NAMA_PELABUHAN_ASAL { get; set; }
        public string NAMA_PELABUHAN_TUJUAN { get; set; }
        public string NO_PKK { get; set; }
        public string NO_PPK1 { get; set; }
        public string NO_PPK_JASA { get; set; }
        public DateTime? PNT_TGL_MULAI { get; set; }
        public DateTime? PNT_TGL_SELESAI { get; set; }
        public string PNT_KD_DMG { get; set; }
        public DateTime? REA_TGL_MULAI { get; set; }
        public DateTime? REA_TGL_SELESAI { get; set; }
        public string REA_KD_DMG { get; set; }
        public string REA_NAMA_DMG { get; set; }
        public string KADE_METER { get; set; }
        public string STATUS { get; set; }
        public string PROGRESS { get; set; }
        public string COLOR { get; set; }
    }
}
