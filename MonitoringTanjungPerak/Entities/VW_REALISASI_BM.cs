﻿namespace MonitoringTanjungPerak.Entities
{
    public class VW_REALISASI_BM
    {
        public string TPPKB1_NOMOR { get; set; }
        public string TPPKB1_NO_FORM { get; set; }
        public string TPPKB1_NAMA_KAPAL { get; set; }
        public string AGEN { get; set; }
        public string PNT_MULAI { get; set; }
        public string PNT_SELESAI { get; set; }
        public string FIRST_LINE { get; set; }
        public string DERMAGA { get; set; }
        public string REC_STAT { get; set; }
        public string PROGRES { get; set; }

        public string MKPL_KODE { get; set; }
        public string MKPL_CALL_SIGN { get; set; }
        public string MSI_CODE { get; set; }
        public string MKPL_JENIS { get; set; }
        
        public string NO_PKK { get; set; }
        public string LOA { get; set; }
        public string GRT { get; set; }

        public int PGRS_BONGKAR { get; set; }
        public int PGRS_MUAT { get; set; }
        public string START_WORK_B { get; set; }
        public string END_WORK_B { get; set; }
        public string START_WORK_M { get; set; }
        public string END_WORK_M { get; set; }
        public string PROGRES_BONGKAR { get; set; }
        public string RENCANA_BONGKAR { get; set; }
        public string REALISASI_BONGKAR { get; set; }
        public string PROGRES_MUAT { get; set; }
        public string RENCANA_MUAT { get; set; }
        public string REALISASI_MUAT { get; set; }
    }


}
