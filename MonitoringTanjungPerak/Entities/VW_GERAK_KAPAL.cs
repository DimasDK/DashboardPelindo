﻿using System;
namespace MonitoringTanjungPerak.Entities
{
    public class VW_GERAK_KAPAL
    { 
        public string KODE_CABANG { get; set; }
        public string NAMA_KAPAL { get; set; }
        public string GT_KAPAL { get; set; }
        public string LOA { get; set; }
        public string MKPL_JENIS { get; set; }
        public string NO_PKK { get; set; }
        public string ID_VISIT { get; set; }
        public string KODE_AGEN { get; set; }
        public string NAMA_AGEN { get; set; }
        public string KODE_PELABUHAN_ASAL { get; set; }
        public string NAMA_PELABUHAN_ASAL { get; set; }
        public string KODE_PELABUHAN_TUJUAN { get; set; }
        public string NAMA_PELABUHAN_TUJUAN { get; set; }
        public string GERAKAN { get; set; }
        public string AREA { get; set; }
        public string NO_PPK1 { get; set; }
        public string JASA { get; set; }
        public string STATUS { get; set; }
        public string PPK_VAR { get; set; }
        public string ID_DP { get; set; }
        public string NO_PPK_JASA { get; set; }
        public string JENIS_GERAKAN { get; set; }
        public DateTime TGL_MULAI_PMH { get; set; }
        public DateTime TGL_SELESAI_PMH { get; set; }
        public string KADE_PMH { get; set; }
        public string KODE_ASAL_PMH { get; set; }
        public string KODE_TUJUAN_PMH { get; set; }
        public string NAMA_ASAL_PMH { get; set; }
        public string NAMA_TUJUAN_PMH { get; set; }
        public DateTime TGL_MULAI_PTP { get; set; }
        public DateTime TGL_SELESAI_PTP { get; set; }
        public string KADE_PTP { get; set; }
        public string KODE_ASAL_PTP { get; set; }
        public string KODE_TUJUAN_PTP { get; set; }
        public string NAMA_ASAL_PTP { get; set; }
        public string NAMA_TUJUAN_PTP { get; set; }
        public DateTime TGL_MULAI_REA { get; set; }
        public DateTime TGL_SELESAI_REA { get; set; }
        public string KADE_REA { get; set; }
        public string KODE_ASAL_REA { get; set; }
        public string KODE_TUJUAN_REA { get; set; }
        public string NAMA_ASAL_REA { get; set; }
        public string NAMA_TUJUAN_REA { get; set; }
        public string NAMA_PANDU { get; set; }
        public DateTime TGL_VERIFIKASI { get; set; }
        public string TOTAL_IDR { get; set; }
    }
}