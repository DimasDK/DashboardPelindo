﻿using System;
namespace MonitoringTanjungPerak.Models
{
    public class VW_VERIFIKASI
    { 
        public int KD_CABANG { get; set; }
        public string KD_VERIFIKASI { get; set; }
        public DateTime TGL_VERIFIKASI { get; set; }
        public int QTY_TUNAI { get; set; }
        public int QTY_EMONEY { get; set; }
        public int QTY_FREEPASS { get; set; }
        public int QTY_LANGGANAN { get; set; }
        public int QTY_TOTAL { get; set; }
        public int TOTAL_TUNAI { get; set; }
        public int TOTAL_EMONEY { get; set; }
        public int TOTAL_PAS_MANUAL { get; set; }
        public int TOTAL_ORANG { get; set; }
        public int TOTAL_KENDARAAN { get; set; }
        public int TOTAL_PENDAPATAN { get; set; }
        public string STATUS_KONFIRMASI { get; set; }
        public int KODE_BAYAR { get; set; }
        public int QTY_KENDARAAN_CASH { get; set; }
        public int QTY_ORANG_CASH { get; set; }
        public int QTY_KENDARAAN_EMONEY { get; set; }
        public int QTY_ORANG_EMONEY { get; set; }
        public int PENDAPATAN_KENDARAAN_CASH { get; set; }
        public int PENDAPATAN_KENDARAAN_EMONEY { get; set; }
        public int PENDAPATAN_ORANG_CASH { get; set; }
        public int PENDAPATAN_ORANG_EMONEY { get; set; }
        public int QTY_LAIN_EMONEY { get; set; }
        public int PENDAPATAN_LAIN_EMONEY { get; set; }
        public int TOTAL_JUMLAH { get; set; }
    }
}