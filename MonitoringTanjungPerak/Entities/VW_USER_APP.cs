﻿namespace MonitoringTanjungPerak.Entities
{
    public class VW_USER_APP
    { 
        public int KD_CABANG { get; set; }
        public int ID { get; set; }
        public string USER_LOGIN { get; set; }
        public string ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
    }
}