﻿namespace MonitoringTanjungPerak.Entities
{
    public class VW_MON_BALANCE_BM
    {
        public string NO_PPKB1_KPL { get; set; }
        public string KEGIATAN { get; set; }
        public string JML_PLAN { get; set; }
        public string JML_REAL_SD { get; set; }
        public string BALANCE { get; set; }
        public string STATUS_DATA { get; set; }
    }
}