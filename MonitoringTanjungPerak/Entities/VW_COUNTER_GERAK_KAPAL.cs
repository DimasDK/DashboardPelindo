﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitoringTanjungPerak.Entities
{
    public class VW_COUNTER_GERAK_KAPAL
    {
        public string KODE_CABANG { get; set; }
        public string NAMA_CABANG { get; set; }
        public string PNT_JENIS_GERAKAN { get; set; }
        public string JML { get; set; }
    }
}