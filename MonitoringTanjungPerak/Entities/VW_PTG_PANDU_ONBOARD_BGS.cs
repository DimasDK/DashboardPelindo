﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitoringTanjungPerak.Entities
{
    public class VW_PTG_PANDU_ONBOARD_BGS
    { 
        public string NAMA_PETUGAS { get; set; }
        public string NO_PPK { get; set; }
        public string KAPAL { get; set; }
        public string KD_PANDU { get; set; }
        public string LOKASI { get; set; }
        public string NO_SPK { get; set; }
        public DateTime TGL_SPK { get; set; }
        public string CABANG { get; set; }
        public string NAMA_CABANG { get; set; }
        public string DARI { get; set; }
        public string KE { get; set; }
        public string JENIS { get; set; }
    }
}