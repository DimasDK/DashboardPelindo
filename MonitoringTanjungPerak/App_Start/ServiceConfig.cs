﻿using Dapper;
using SatuMedia.Data;
using System.Data;

namespace MonitoringTanjungPerak.App_Start
{
    public class ServiceConfig
    {
        private static string[] databaseKeys = new[] { "Default", "DbLite" };
        public static IDbConnection DefaultDbConnection { get; set; }

        public static void CreateDatabaseConnection()
        {
            GlobalRepository.SetDialect(GlobalRepository.Dialect.Oracle);
            DefaultDbConnection = SqlConnections.NewByKey(databaseKeys[0]);
        }
    }
}