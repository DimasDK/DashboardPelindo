﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace MonitoringTanjungPerak
{
    public class AppUser : ClaimsPrincipal
    {
        public AppUser(ClaimsPrincipal principal)
            : base(principal)
        {
        }

        public string Name
        {
            get
            {
                return this.FindFirst(ClaimTypes.Name).Value;
            }
        }
        public int UserId
        {
            get
            {
                return int.Parse(this.FindFirst("USER_ID").Value);
                //return this.FindFirst(ClaimTypes.US).Value;
            }
        }
        public string KodeCabang
        {
            get
            {
                return this.FindFirst("KD_CABANG").Value;
            }
        }
        public string RoleID
        {
            get
            {
                return this.FindFirst("ROLE_ID").Value;
            }
        }
        public string KodeCabangOrig
        {
            get
            {
                return this.FindFirst("KD_CABANG_ORIG").Value;
            }
        }
    }
}