﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace MonitoringTanjungPerak
{
    public class AuthActivity : AuthorizeAttribute
    {
        public string Activity { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            base.AuthorizeCore(httpContext);

            var principal = (ClaimsPrincipal)Thread.CurrentPrincipal;
            var claims = principal.Claims;
            var userActivities = new List<string>();

            foreach (var item in claims)
            {
                if (item.Type == ClaimTypes.Role)
                {
                    userActivities.Add(item.Value);
                }
            }

            return userActivities.Contains(Activity) ? true : false;
        }

        internal class Http403Result : ActionResult
        {
            public override void ExecuteResult(ControllerContext context)
            {
                context.HttpContext.Response.StatusCode = 403;
            }
        }

        // Untuk AJAX
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            // Do ajax checking here
            filterContext.Result = new Http403Result();
        }
    }
}