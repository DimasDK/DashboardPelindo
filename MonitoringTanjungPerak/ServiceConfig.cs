﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Oracle.ManagedDataAccess.Client;

namespace MonitoringTanjungPerak
{
    public class ServiceConfig
    {
        private static string[] databaseKeys = new[] { "Default", "DbLite", "TOSGC_WHM" };
        public static OracleConnection DefaultDbConnection { get; set; }
        public static OracleConnection AppRepoDbConnection { get; set; }
        public static OracleConnection TosWHMDbConnection { get; set; }
        public static void CreateDatabaseConnection()
        {
            DefaultDbConnection = CreateOracleDbConnection(databaseKeys[0]);
            TosWHMDbConnection = CreateOracleDbConnection(databaseKeys[2]);
        }


        private static OracleConnection CreateOracleDbConnection(string connectionName)
        {
            try
            {
                var connectionSetting = System.Configuration.ConfigurationManager.
                ConnectionStrings[connectionName].ConnectionString;
                var conn = new OracleConnection(connectionSetting);
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {

                    throw;
                }
                return conn;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static void CreateProductionDbConnection()
        {
            try
            {
                var connectionSetting = System.Configuration.ConfigurationManager.
                ConnectionStrings["OracleProdDbContext"].ConnectionString;
                var conn = new OracleConnection(connectionSetting);
                conn.Open();
                MasterDataDbConnection = conn;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static OracleConnection MasterDataDbConnection { get; set; }
    }
}