﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitoringTanjungPerak.ViewModels
{
    public class ReturnDataTables<T>
    {
        public List<T> data { get; set; }
        public List<ReportHeaderDefModel> columns { get; set; }
    }
}