﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitoringTanjungPerak.ViewModels
{
    public class ReportHeaderDefModel
    {
        public string title { get; set; }
        public string data { get; set; }
    }
}