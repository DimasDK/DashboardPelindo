﻿using MonitoringTanjungPerak.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;

namespace MonitoringTanjungPerak.DAL
{
    public class PanduDAL
    {
        public List<VW_COUNTER_PANDU> GetCounterPandu()
        {
            List<VW_COUNTER_PANDU> result = new List<VW_COUNTER_PANDU>();

            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = "SELECT KODE_CABANG, NAMA_CABANG, JENIS, JML FROM VW_PTG_PANDU_COUNT_BGS";
                result = connection.Query<VW_COUNTER_PANDU>(sql).ToList();

            }
            catch (Exception e)
            {
                string ex = e.Message.ToString();
            }
            connection.Close();
            return result;
        }

        public List<VW_PTG_PANDU_ONBOARD_BGS> GetDataPanduOnboard(string xCABANG, string xNAMA_CABANG)
        {
            List<VW_PTG_PANDU_ONBOARD_BGS> result = new List<VW_PTG_PANDU_ONBOARD_BGS>();

            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = @"SELECT NAMA_PETUGAS, NO_PPK, KAPAL, KD_PANDU, LOKASI, NO_SPK, TGL_SPK, CABANG, NAMA_CABANG, DARI, KE, 
                JENIS FROM VW_PTG_PANDU_ONBOARD_BGS WHERE CABANG=:CABANG AND NAMA_CABANG=:NAMA_CABANG";
                result = connection.Query<VW_PTG_PANDU_ONBOARD_BGS>(sql, new {
                    CABANG = xCABANG,
                    NAMA_CABANG = xNAMA_CABANG
                }).ToList();

            }
            catch (Exception e)
            {
                string ex = e.Message.ToString();
            }
            connection.Close();
            return result;
        }

        public List<VW_PTG_PANDU_ONDUTY_BGS> GetDataPanduOnduty(string xCABANG, string xNAMA_CABANG)
        {
            List<VW_PTG_PANDU_ONDUTY_BGS> result = new List<VW_PTG_PANDU_ONDUTY_BGS>();

            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = @"SELECT NAMA_PETUGAS, NO_PPK, KAPAL, KD_PANDU, LOKASI, NO_SPK, TGL_SPK, CABANG, NAMA_CABANG, DARI, KE, 
                JENIS FROM VW_PTG_PANDU_ONDUTY_BGS WHERE CABANG=:CABANG AND NAMA_CABANG=:NAMA_CABANG";
                result = connection.Query<VW_PTG_PANDU_ONDUTY_BGS>(sql, new
                {
                    CABANG = xCABANG,
                    NAMA_CABANG = xNAMA_CABANG
                }).ToList();

            }
            catch (Exception e)
            {
                string ex = e.Message.ToString();
            }
            connection.Close();
            return result;
        }

        public List<VW_PTG_PANDU_STANDBY_BGS> GetDataPanduStandby(string xCABANG, string xNAMA_CABANG)
        {
            List<VW_PTG_PANDU_STANDBY_BGS> result = new List<VW_PTG_PANDU_STANDBY_BGS>();

            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = @"SELECT NAMA_PETUGAS, NO_PPK, KAPAL, KD_PANDU, LOKASI, NO_SPK, TGL_SPK, CABANG, NAMA_CABANG, DARI, KE, 
                JENIS FROM VW_PTG_PANDU_STANDBY_BGS WHERE CABANG=:CABANG AND NAMA_CABANG=:NAMA_CABANG";
                result = connection.Query<VW_PTG_PANDU_STANDBY_BGS>(sql, new
                {
                    CABANG = xCABANG,
                    NAMA_CABANG = xNAMA_CABANG
                }).ToList();

            }
            catch (Exception e)
            {
                string ex = e.Message.ToString();
            }
            connection.Close();
            return result;
        }
    }
}