﻿using Dapper;
using MonitoringTanjungPerak.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using MonitoringTanjungPerak.Helpers;
using MonitoringTanjungPerak.Models;

namespace MonitoringTanjungPerak.DAL
{
    public class AuthDAL
    {
        public APP_USER GetUserInformation(string username, string password)
        {
            string _password = string.Empty;
            string _unProtect = string.Empty;
            StringProtector sp = new StringProtector();
            DecEncrypt de = new DecEncrypt();
            APP_USER result = new APP_USER();
            //xAkun = xPortal.valLoginAkun("99", username, password);
            //if (xAkun.HAKAKSES != "-")
            //{
            //    result.ID = xAkun.IDUSER;
            //    result.USER_LOGIN = xAkun.USERNAME;
            //    result.USER_ROLE_ID = xAkun.HAKAKSES;
            //    result.KD_CABANG = xAkun.KD_CABANG;
            //    result.USER_NAMA = xAkun.NAMA;
            //    result.NAMA_CABANG = xAkun.NAMA_CABANG;
            //    return result;
            //    //string sql = "SELECT * " +
            //    //             "FROM app_user " +
            //    //             "WHERE user_login = :a";

            //    //IEnumerable<APP_USER> queryResult = connection.Query<APP_USER>(sql, new
            //    //{
            //    //    a = username
            //    //});

            //    //if (queryResult.Count() == 1)
            //    //{
            //    //    result = queryResult.FirstOrDefault();
            //    //    _unProtect = de.Decrypt(result.USER_PASSWORD);
            //    //    //_unProtect = sp.Unprotect(result.USER_PASSWORD);
            //    //    if (!password.Equals(_unProtect)) result = null;
            //    //    return result;
            //    //}
            //}
            return null;
        }
        
        public IEnumerable<MainMenu> GenerateMainMenuList(string xROLE_ID)
        {
            var sql = "SELECT '' KD_CABANG, '' ID, R.ROLE_ID,R.ROLE_NAME,M.MENU_NAMA,M.MENU_URL,M.MENU_ICON, " +
                "M.MENU_ACTIVITY, MP.PERMISSION_ADD, MP.PERMISSION_DEL, MP.PERMISSION_UPD, MP.PERMISSION_VIW, " +
                "(SELECT COUNT(-1) FROM APP_MENU XM,APP_USER_MENU_PORTALSI XUM " +
                "WHERE XM.MENU_ID = XUM.MENU_ID AND XUM.ROLE_ID=:ID " +
                "AND XUM.MENU_PARENT_ID = M.MENU_ID AND XUM.STATUS = 1 ) " +
                "AS CHILD_COUNT, M.MENU_ID,UM.MENU_PARENT_ID,LEVEL as LEVEL_ID " +
                "FROM APP_ROLE R, APP_MENU_PERMISSION MP, " +
                "(SELECT * FROM APP_MENU WHERE MENU_VISIBLE = 1 " +
                "AND MENU_STATUS = 1 AND MENU_GROUP_ID = 1) M, " +
                "(SELECT * FROM APP_USER_MENU_PORTALSI WHERE STATUS = 1 AND ROLE_ID=:ID) UM " +
                "WHERE R.ROLE_ID = MP.ROLE_ID AND MP.MENU_ID = M.MENU_ID AND R.ROLE_ID=:ID " +
                "AND M.MENU_ID = UM.MENU_ID AND R.ROLE_ID = UM.ROLE_ID   " +
                "START WITH MENU_PARENT_ID = 0 " +
                "CONNECT BY PRIOR MP.MENU_ID = UM.MENU_PARENT_ID " +
                "ORDER SIBLINGS BY ROLE_ID, ROLE_NAME ";
            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            var res = connection.Query<MainMenu>(sql, new { ID = xROLE_ID });
            return res;
        }

        public IEnumerable<MainMenu> GenerateRoroMenuList(string xROLE_ID)
        {
            var sql = "SELECT '' KD_CABANG, '' ID, R.ROLE_ID,R.ROLE_NAME,M.MENU_NAMA,M.MENU_URL,M.MENU_ICON, " +
                "M.MENU_ACTIVITY, MP.PERMISSION_ADD, MP.PERMISSION_DEL, MP.PERMISSION_UPD, MP.PERMISSION_VIW, " +
                "(SELECT COUNT(-1) FROM APP_MENU XM,APP_USER_MENU_PORTALSI XUM " +
                "WHERE XM.MENU_ID = XUM.MENU_ID AND XUM.ROLE_ID=:ID " +
                "AND XUM.MENU_PARENT_ID = M.MENU_ID AND XUM.STATUS = 1 ) " +
                "AS CHILD_COUNT, M.MENU_ID,UM.MENU_PARENT_ID,LEVEL as LEVEL_ID " +
                "FROM APP_ROLE R, APP_MENU_PERMISSION MP, " +
                "(SELECT * FROM APP_MENU WHERE MENU_VISIBLE = 1 " +
                "AND MENU_STATUS = 1 AND MENU_GROUP_ID = 2) M, " +
                "(SELECT * FROM APP_USER_MENU_PORTALSI WHERE STATUS = 1 AND ROLE_ID=:ID) UM " +
                "WHERE R.ROLE_ID = MP.ROLE_ID AND MP.MENU_ID = M.MENU_ID AND R.ROLE_ID=:ID " +
                "AND M.MENU_ID = UM.MENU_ID AND R.ROLE_ID = UM.ROLE_ID   " +
                "START WITH MENU_PARENT_ID = 0 " +
                "CONNECT BY PRIOR MP.MENU_ID = UM.MENU_PARENT_ID " +
                "ORDER SIBLINGS BY ROLE_ID, ROLE_NAME ";
            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            var res = connection.Query<MainMenu>(sql, new { ID = xROLE_ID });
            return res;
        }
        
    }
}