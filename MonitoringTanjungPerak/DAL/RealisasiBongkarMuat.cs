﻿using Dapper;
using MonitoringTanjungPerak.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MonitoringTanjungPerak.Models;
using System.Configuration;

namespace MonitoringTanjungPerak.DAL
{
    public class RealisasiBongkarMuat
    {
        public VW_REALISASI_BM getDataDetailsKegiatanKapal(string tppkb1Nomor)
        {
            VW_REALISASI_BM result = new VW_REALISASI_BM();
            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = "select temps.*, "+
                                "	case  " +
                                "	when FIRST_LINE is null then 0 " +
                                "	when FIRST_LINE is not null then 		 " +
                                "		round((round((sysdate - FIRST_LINE) * (60 * 24))/ " +
                                "    round((pnt_selesai - FIRST_LINE) * (60 * 24)))*100) " +
                                "		end as progres " +
                                "from ( " +
                                "    select " +
                                "    a.tppkb1_nomor, " +
                                "    a.tppkb1_no_form, " +
                                "    k.mkpl_kode," +
                                "    a.tppkb1_nama_kapal, " +
                                "    k.MKPL_CALL_SIGN, " +
                                "    k.MSI_CODE, " +
                                "    k.MKPL_JENIS, " +
                                "          (select a.mplg_kode||' - '||mplg_nama from safm_pelanggan@mdm_prod where mplg_kode = a.mplg_kode and kd_cabang ='2')agen, " +
                                "    min(b.pntmb_jammtambat)pnt_mulai, max(b.pntmb_jamstambat)pnt_selesai, " +
                                "    (select min(BKTBT_JAMMTAMBAT) from upkt_bukti_tambat where tppkb1_nomor = a.tppkb1_nomor and rec_stat in ('B','P'))FIRST_LINE, " +
                                "    (select mdmg_kode||' - '||mdmg_nama from upkm_dermaga where mdmg_kode = b.mdmg_kode)dermaga, " +
                                "    b.rec_stat , " +
                                "    a.NO_PKK," +
                                "    a.tppkb1_loa as LOA," +
                                "    a.tppkb1_grt as GRT " +
                                "    from upkt_pmh_ppkb1 a,upkt_pnt_tambat b, upkm_kapal k" +
                                "    where " +
                                "    a.tppkb1_nomor = b.tppkb1_nomor " +
                                "    and k.mkpl_kode = a.mkpl_kode " +
                                "    and b.rec_stat in ('B','P') " +
                                "    and a.tppkb1_nomor = :PARAMS " +
                                "    and to_date(to_char(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy') between to_date(to_char(b.pntmb_tglmtambat-1,'dd/mm/yyyy'),'dd/mm/yyyy') " +
                                "    and to_date(to_char(b.pntmb_tglstambat,'dd/mm/yyyy'),'dd/mm/yyyy')  " +
                                "    group by k.mkpl_kode, k.MKPL_CALL_SIGN, k.MSI_CODE, k.MKPL_JENIS, a.tppkb1_nomor,a.tppkb1_nama_kapal,b.mdmg_kode,b.rec_stat,a.mplg_kode, a.tppkb1_no_form, a.NO_PKK, a.tppkb1_loa, a.tppkb1_grt " +
                                "    order by 6 " +
                                ")temps";

                VW_REALISASI_BM rsDM = connection.Query<VW_REALISASI_BM>(sql, new { PARAMS = tppkb1Nomor }).SingleOrDefault();
                
                if ((rsDM.TPPKB1_NO_FORM != null) && (rsDM.TPPKB1_NO_FORM != ""))
                {
                    try
                    {

                        string sqlBM = "select distinct  " +
                            "	genc.NO_PPK as TPPKB1_NO_FORM, " +
                            "	genc.NAMA_KAPAL as MKPL_NAMA, " +
                            "    case  " +
                            "		when genc.KEGIATAN = 'BONGKAR' then  " +
                            "        round((round((sysdate - genc.START_WORK) * (60 * 24))/ " +
                            "        round((genc.END_WORK - genc.START_WORK) * (60 * 24)))*100) " +
                            "			else 0 " +
                            "		end as PGRS_BONGKAR, " +
                            "    case  " +
                            "		when genc.KEGIATAN = 'MUAT' then  " +
                            "        round((round((sysdate - genc.START_WORK) * (60 * 24))/ " +
                            "        round((genc.END_WORK - genc.START_WORK) * (60 * 24)))*100) " +
                            "			else 0 " +
                            "		end as PGRS_MUAT, " +
                            "    case  " +
                            "		when genc.KEGIATAN = 'BONGKAR' then " +
                            "        to_char(genc.START_WORK, 'dd-mm-rrrr hh24:mi') " +
                            "      else '' " +
                            "    end as start_work_b, " +
                            "    case  " +
                            "		when genc.KEGIATAN = 'BONGKAR' then " +
                            "        to_char(genc.END_WORK, 'dd-mm-rrrr hh24:mi') " +
                            "      else '' " +
                            "    end as end_work_b, " +
                            "    case  " +
                            "		when genc.KEGIATAN = 'MUAT' then " +
                            "        to_char(genc.START_WORK, 'dd-mm-rrrr hh24:mi') " +
                            "      else '' " +
                            "    end as start_work_m, " +
                            "    case  " +
                            "		when genc.KEGIATAN = 'MUAT' then " +
                            "        to_char(genc.END_WORK, 'dd-mm-rrrr hh24:mi') " +
                            "      else '' " +
                            "    end as end_work_m, " +
                            "	case  " +
                            "		when genc.KEGIATAN = 'BONGKAR' then  " +
                            "			round((genc.JML_PROD / genc.JML_PLAN)*100)  " +
                            "		else 0 " +
                            "	end as PROGRES_BONGKAR,   " +
                            "	case when genc.KEGIATAN = 'BONGKAR' then genc.JML_PLAN else 0 end as RENCANA_BONGKAR, " +
                            "	case when genc.KEGIATAN = 'BONGKAR' then genc.JML_PROD else 0 end as REALISASI_BONGKAR, " +
                            "	case  " +
                            "		when genc.KEGIATAN = 'MUAT' then  " +
                            "			round((genc.JML_PROD / genc.JML_PLAN)*100)  " +
                            "		else 0 " +
                            "	end as PROGRES_MUAT,  " +
                            "	case when genc.KEGIATAN = 'MUAT' then genc.JML_PLAN else 0 end as RENCANA_MUAT, " +
                            "	case when genc.KEGIATAN = 'MUAT' then genc.JML_PROD else 0 end as REALISASI_MUAT " +
                            "from TOSGC.V_MON_REAL_BALANCING@MDM_PROD genc " +
                            "where kd_cabang = 2 and no_ppk = :PARAMS_NOFORM";

                        
                        VW_REALISASI_BM rsBM = connection.Query<VW_REALISASI_BM>(sqlBM, new { PARAMS_NOFORM = rsDM.TPPKB1_NO_FORM }).SingleOrDefault();
                        if(rsBM.TPPKB1_NO_FORM != null) {
                            rsDM.PGRS_BONGKAR = rsBM.PGRS_BONGKAR;
                            rsDM.PGRS_MUAT = rsBM.PGRS_MUAT;

                            rsDM.START_WORK_B = (rsBM.START_WORK_B != null) ? rsBM.START_WORK_B.ToString() : "";
                            rsDM.END_WORK_B = (rsBM.END_WORK_B != null) ? rsBM.END_WORK_B.ToString() : "";

                            rsDM.START_WORK_M = (rsBM.START_WORK_M != null) ? rsBM.START_WORK_M.ToString() : "";
                            rsDM.END_WORK_M = (rsBM.END_WORK_M != null) ? rsBM.END_WORK_M.ToString() : "";

                            rsDM.PROGRES_BONGKAR = rsBM.PROGRES_BONGKAR.ToString();
                            rsDM.RENCANA_BONGKAR = rsBM.RENCANA_BONGKAR.ToString();
                            rsDM.REALISASI_BONGKAR = rsBM.REALISASI_BONGKAR.ToString();
                            rsDM.PROGRES_MUAT = rsBM.PROGRES_MUAT.ToString();
                            rsDM.RENCANA_MUAT = rsBM.RENCANA_MUAT.ToString();
                            rsDM.REALISASI_MUAT = rsBM.REALISASI_MUAT.ToString();
                        }
                    }
                    catch (Exception e)
                    {
                        string ez = e.Message.ToString();
                    }
                }
                connection.Close();

                result = rsDM;
            }
            catch (Exception e)
            {
                string ex = e.Message.ToString();
            }
            connection.Close();
            return result;
        }

        public List<VW_MONITORING> getDataMoreDetailsKapal(string tppkb1Nomor)
        {
            List<VW_MONITORING> result = new List<VW_MONITORING>();
            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = "select * from ( " +
                                    "	select  " +
                                    "		a.PMH_TPPKB1,  " +
                                    "		a.PMH_NO_FORM TPPKB1_NO_FORM,  " +
                                    "		a.PMH_JNS_JASA,  " +
                                    "		to_char(nvl(a.pnt_mulai, a.pmh_mulai), 'dd-mm-rrrr hh24:mi') rcn_mulai,  " +
                                    "		to_char(nvl(a.pnt_selesai, a.pmh_selesai), 'dd-mm-rrrr hh24:mi') rcn_selesai,  " +
                                    "		to_char(a.bkt_mulai, 'dd-mm-rrrr hh24:mi') tgl_mulai,  " +
                                    "		to_char(a.bkt_selesai, 'dd-mm-rrrr hh24:mi') tgl_selesai, " +
                                    "		nvl(nvl(a.bkt_lok_awal, a.pnt_lok_awal),a.pmh_lok_awal)lok_awal,  " +
                                    "		nvl(nvl(a.bkt_lok_akhir, a.pnt_lok_akhir),a.pmh_lok_akhir)lok_akhir,  " +
                                    "		(select ds.mdmg_nama from upkm_dermaga ds where ds.kd_cabang = 2 and ds.mdmg_kode = nvl(nvl(a.bkt_lok_awal, a.pnt_lok_awal),a.pmh_lok_awal))Dmg_awal,  " +
                                    "		(select de.mdmg_nama from upkm_dermaga de where de.kd_cabang = 2 and de.mdmg_kode = nvl(nvl(a.bkt_lok_akhir, a.pnt_lok_akhir),a.pmh_lok_akhir))Dmg_akhir, " +
                                    "		PMH_REC_SAT REC_STAT , "+
                                    "    case " +
                                    "      when BKT_KDAWAL is not null and BKT_KDAKHIR is not null then BKT_KDAWAL " +
                                    "      else PNT_KDAWAL " +
                                    "    end KDAWAL, " +
                                    "    case " +
                                    "      when BKT_KDAWAL is not null and BKT_KDAKHIR is not null then BKT_KDAKHIR " +
                                    "      else PNT_KDAKHIR " +
                                    "    end KDAKHIR " +
                                    "	from view_monitoring a " +
                                    "		where a.PMH_TPPKB1 = :PARAMS " +
                                    "	union all " +
                                    "	select " +
                                    "	  but.TPPKB1_NOMOR PMH_TPPKB1, " +
                                    "	  BTUBTMB_NO_FORM PMH_NO_FORM, " +
                                    "	  'BTLUBH_TAMBAT' PMH_JNS_JASA, " +
                                    "	  to_char(BTUBTMB_JAMMTAMBAT, 'dd-mm-rrrr hh24:mi') rcn_mulai,  " +
                                    "	  to_char(BTUBTMB_JAMSTAMBAT, 'dd-mm-rrrr hh24:mi') rcn_selesai, " +
                                    "	  null tgl_mulai,  " +
                                    "	  null tgl_selesai, " +
                                    "	  null lok_awal,  " +
                                    "	  but.mdmg_kode lok_akhir, " +
                                    "	  null DMG_AWAL, " +
                                    "	  (select dbut.mdmg_nama from upkm_dermaga dbut where dbut.kd_cabang = 2 and dbut.mdmg_kode = but.mdmg_kode)Dmg_akhir,  " +
                                    "	  but.REC_STAT, null, null" +
                                    "	from upkt_btlubh_tambat but " +
                                    "		where but.TPPKB1_NOMOR = :PARAMS " +
                                    "	union all " +
                                    "	select " +
                                    "	  bup.TPPKB1_NOMOR PMH_TPPKB1, " +
                                    "	  BTUBPND_NO_FORM PMH_NO_FORM, " +
                                    "	  'BTLUBH_PANDU' PMH_JNS_JASA, " +
                                    "	  to_char(BTUBPND_JAMMPANDU, 'dd-mm-rrrr hh24:mi') rcn_mulai,  " +
                                    "	  to_char(null, 'dd-mm-rrrr hh24:mi') rcn_selesai, " +
                                    "	  null tgl_mulai,  " +
                                    "	  null tgl_selesai, " +
                                    "	  bup.mdmg_asal_kode lok_awal, " +
                                    "	  bup.mdmg_tujuan_kode lok_akhir, " +
                                    "	  (select dbuts.mdmg_nama from upkm_dermaga dbuts where dbuts.kd_cabang = 2 and dbuts.mdmg_kode = bup.mdmg_asal_kode)DMG_AWAL, " +
                                    "	  (select dbutf.mdmg_nama from upkm_dermaga dbutf where dbutf.kd_cabang = 2 and dbutf.mdmg_kode = bup.mdmg_tujuan_kode)Dmg_akhir, " +
                                    "	  bup.REC_STAT , null, null" +
                                    "	from upkt_btlubh_pandu bup " +
                                    "		where bup.TPPKB1_NOMOR = :PARAMS " +
                                    ")temp " +
                                    "order by 4 ";

                result = connection.Query<VW_MONITORING>(sql, new { PARAMS = tppkb1Nomor }).ToList();
                connection.Close();
            }
            catch (Exception e)
            {
                string ex = e.Message.ToString();
            }
            connection.Close();
            return result;
        }

        public IEnumerable<VW_MON_BALANCE_BM> GetDataBalanceBM(string KodeCbg, string Noppk)
        {
            IDbConnection connection = ServiceConfig.TosWHMDbConnection;
            IEnumerable<VW_MON_BALANCE_BM> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT KD_CABANG, KD_TERMINAL, NO_PPKB1_KPL, KEGIATAN, JML_PLAN, JML_REAL_SD, BALANCE, 'GEN-C' STATUS_DATA " +
                             "FROM VW_MON_BALANCE_GENC WHERE NO_PPKB1_KPL=:NO_PPKB1_KPL AND LPAD(KD_CABANG,2,'0')=:KD_CABANG";

                listData = connection.Query<VW_MON_BALANCE_BM>(sql, new { NO_PPKB1_KPL = Noppk, KD_CABANG = KodeCbg });
            }
            catch (Exception e)
            {
                listData = null;
            }

            if (listData.Count()==0)
            {
                try
                {
                    string sql = "SELECT KD_CABANG, KD_TERMINAL, NO_PPKB1_KPL, KEGIATAN, JML_PLAN, JML_REAL_SD, BALANCE, 'SPINER' STATUS_DATA " +
                                 "FROM VW_MON_BALANCE_SPNR WHERE NO_PPKB1_KPL=:NO_PPKB1_KPL AND LPAD(KD_CABANG,2,'0')=:KD_CABANG";

                    listData = connection.Query<VW_MON_BALANCE_BM>(sql, new { NO_PPKB1_KPL = Noppk, KD_CABANG = KodeCbg });
                }
                catch (Exception e)
                {
                    listData = null;
                }
            }
            connection.Close();

            return listData;
        }

    }
}