﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MonitoringTanjungPerak.ViewHelper;

namespace MonitoringTanjungPerak.DAL
{
    public class ViewHelperDAL
    {
        public IEnumerable<VW_TerminalModel> GetDataTerminal(string xKD_CABANG, string xPARAM)
        {
            VW_TerminalModel result = null;
            IDbConnection connection = ServiceConfig.TosWHMDbConnection;
            IEnumerable<VW_TerminalModel> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT KD_TERMINAL id, KD_TERMINAL, NAMA_TERMINAL FROM VW_TERMINAL " +
                             "WHERE KD_CABANG = :KD_CABANG AND UPPER(NAMA_TERMINAL) LIKE '%' || UPPER(:PARAM) || '%' ";
                listData = connection.Query<VW_TerminalModel>(sql, new {KD_CABANG = xKD_CABANG, PARAM = xPARAM});
            }
            catch (Exception)
            {
                listData = null; 
            }
            connection.Close();

            return listData;
        }

        public IEnumerable<VW_TerminalModel> GetDataTerminalByCabang(string xKD_CABANG)
        {
            VW_TerminalModel result = null;
            IDbConnection connection = ServiceConfig.TosWHMDbConnection;
            IEnumerable<VW_TerminalModel> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT MDMG_KODE id, MDMG_KODE, MDMG_NAMA NAMA_TERMINAL FROM VW_DERMAGA " +
                             "WHERE KD_CABANG = :KD_CABANG";
                listData = connection.Query<VW_TerminalModel>(sql, new { KD_CABANG = xKD_CABANG});
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();

            return listData;
        }

        public IEnumerable<VW_PelabuhanModel> GetDataPelabuhan(string xPARAM)
        {
            VW_PelabuhanModel result = null;
            IDbConnection connection = ServiceConfig.TosWHMDbConnection;
            IEnumerable<VW_PelabuhanModel> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT MPLB_KODE id, MPLB_KODE, MPLB_NAMA, MPLB_KOTA, MNEG_KODE FROM VW_PELABUHAN " +
                             "WHERE UPPER(MPLB_NAMA||MPLB_KOTA) LIKE '%' || UPPER(:PARAM) || '%' ";
                listData = connection.Query<VW_PelabuhanModel>(sql, new { PARAM = xPARAM });
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();

            return listData;
        }

        public IEnumerable<VW_Cabang> GetDataCabang()
        {

            IDbConnection connection = ServiceConfig.TosWHMDbConnection;
            IEnumerable<VW_Cabang> listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT LPAD(KODE_CABANG,2,'0') KODE_CABANG, NAMA_PELB FROM VW_CABANG WHERE KODE_CABANG IN ('2') ";
                listData = connection.Query<VW_Cabang>(sql);
            }
            catch (Exception)
            {
                listData = null;
            }
            connection.Close();
            return listData;
        }
    }
}