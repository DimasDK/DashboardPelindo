﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace MonitoringTanjungPerak.DAL
{
    public class DatabaseHelper
    {
        public static string GetConnectionString(string name = "default")
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }
    }
}