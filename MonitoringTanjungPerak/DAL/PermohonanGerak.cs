﻿using Dapper;
using MonitoringTanjungPerak.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MonitoringTanjungPerak.Models;
using System.Configuration;

namespace MonitoringTanjungPerak.DAL
{
    public class PermohonanGerak
    {
        public class DataListGerakanKapal
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<VW_GERAK_KAPAL> data { get; set; }
            public string mesaage { get; set; }
        }

        public List<VW_COUNTER_GERAK_KAPAL> GetCounterGerakKapal()
        {
            List<VW_COUNTER_GERAK_KAPAL> result = new List<VW_COUNTER_GERAK_KAPAL>();

            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = "SELECT KODE_CABANG, NAMA_CABANG, PNT_JENIS_GERAKAN, JML FROM VW_MON_GRK_PANDU_COUNT_BGS";
                result = connection.Query<VW_COUNTER_GERAK_KAPAL>(sql).ToList();

            }
            catch (Exception e)
            {
                string ex = e.Message.ToString();
            }
            connection.Close();
            return result;
        }

        public DataListGerakanKapal GetDataGerakanKapal(int draw, int start, int length, string search, string movType, string locCode)
        {
            int count = 0;
            int end = start + length;
            DataListGerakanKapal result = new DataListGerakanKapal();
            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            IEnumerable<VW_GERAK_KAPAL> listData = null;
            
            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                if (string.IsNullOrEmpty(search))
                {
                    string sql = @"select KODE_CABANG, NAMA_KAPAL, GT_KAPAL, LOA, MKPL_JENIS, NO_PKK, ID_VISIT, KODE_AGEN, NAMA_AGEN, 
                        KODE_PELABUHAN_ASAL, NAMA_PELABUHAN_ASAL, KODE_PELABUHAN_TUJUAN, NAMA_PELABUHAN_TUJUAN, GERAKAN, 
                        AREA, NO_PPK1, JASA, STATUS, PPK_VAR, ID_DP, NO_PPK_JASA, JENIS_GERAKAN, TGL_MULAI_PMH, TGL_SELESAI_PMH, 
                        KADE_PMH, KODE_ASAL_PMH, KODE_TUJUAN_PMH, NAMA_ASAL_PMH, NAMA_TUJUAN_PMH, TGL_MULAI_PTP, TGL_SELESAI_PTP, 
                        KADE_PTP, KODE_ASAL_PTP, KODE_TUJUAN_PTP, NAMA_ASAL_PTP, NAMA_TUJUAN_PTP, TGL_MULAI_REA, TGL_SELESAI_REA, 
                        KADE_REA, KODE_ASAL_REA, KODE_TUJUAN_REA, NAMA_ASAL_REA, NAMA_TUJUAN_REA, NAMA_PANDU, TGL_VERIFIKASI, 
                        TOTAL_IDR from VW_MON_GRK_PANDU_BGS where AREA = :ARCODE and GERAKAN = UPPER(:GRKAN)";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<VW_GERAK_KAPAL>(fullSql, new { a = start, b = end, ARCODE = locCode, GRKAN = movType });
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new {ARCODE = locCode, GRKAN = movType });
                }
                else
                {
                    string sql = @"select KODE_CABANG, NAMA_KAPAL, GT_KAPAL, LOA, MKPL_JENIS, NO_PKK, ID_VISIT, KODE_AGEN, NAMA_AGEN, 
                        KODE_PELABUHAN_ASAL, NAMA_PELABUHAN_ASAL, KODE_PELABUHAN_TUJUAN, NAMA_PELABUHAN_TUJUAN, GERAKAN, 
                        AREA, NO_PPK1, JASA, STATUS, PPK_VAR, ID_DP, NO_PPK_JASA, JENIS_GERAKAN, TGL_MULAI_PMH, TGL_SELESAI_PMH, 
                        KADE_PMH, KODE_ASAL_PMH, KODE_TUJUAN_PMH, NAMA_ASAL_PMH, NAMA_TUJUAN_PMH, TGL_MULAI_PTP, TGL_SELESAI_PTP, 
                        KADE_PTP, KODE_ASAL_PTP, KODE_TUJUAN_PTP, NAMA_ASAL_PTP, NAMA_TUJUAN_PTP, TGL_MULAI_REA, TGL_SELESAI_REA, 
                        KADE_REA, KODE_ASAL_REA, KODE_TUJUAN_REA, NAMA_ASAL_REA, NAMA_TUJUAN_REA, NAMA_PANDU, TGL_VERIFIKASI, 
                        TOTAL_IDR from VW_MON_GRK_PANDU_BGS where AREA = :ARCODE and GERAKAN = UPPER(:GRKAN) and UPPER(NAMA_KAPAL||NO_PKK||NAMA_AGEN) like '%'|| UPPER(:PARAM) || '%'";
                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<VW_GERAK_KAPAL>(fullSql, new { a = start, b = end, PARAM = search.ToUpper(), ARCODE = locCode, GRKAN = movType });
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { PARAM = search.ToUpper(), ARCODE = locCode, GRKAN = movType });
                }
            }
            catch (Exception e)
            {
                listData = new List<VW_GERAK_KAPAL>();
            }
            connection.Close();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();

            return result;
        }

    }
}