﻿using Dapper;
using MonitoringTanjungPerak.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MonitoringTanjungPerak.Models;
using System.Configuration;

namespace MonitoringTanjungPerak.DAL
{
    public class PermohonanTambat
    {
        public class DataListPermohonanTambat
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<VW_PENETAPAN_TAMBAT> data { get; set; }
            public string mesaage { get; set; }
        }

        public class DataListVesselProgres
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<VW_MON_VESPROG_BGS> data { get; set; }
            public string mesaage { get; set; }
        }

        public class DataListVesselTambat
        {
            public int draw { get; set; }
            public int recordsTotal { get; set; }
            public int recordsFiltered { get; set; }
            public List<VW_MON_VESTAMBAT_BGS> data { get; set; }
            public string mesaage { get; set; }
        }

        public class DataDataDetailsKegiatanKapal
        {
            public string TPPKB1_NOMOR { get; set; }
            public string TPPKB1_NO_FORM { get; set; }
            public string TPPKB1_NAMA_KAPAL { get; set; }
            public string AGEN { get; set; }
            public string PNT_MULAI { get; set; }
            public string PNT_SELESAI { get; set; }
            public string FIRST_LINE { get; set; }
            public string DERMAGA { get; set; }
            public string REC_STAT { get; set; }
            public string PROGRES { get; set; }
            public string START_WORK { get; set; }
            public string END_WORK { get; set; }
            public string PROGRES_BONGKAR { get; set; }
            public string RENCANA_BONGKAR { get; set; }
            public string REALISASI_BONGKAR { get; set; }
            public string PROGRES_MUAT { get; set; }
            public string RENCANA_MUAT { get; set; }
            public string REALISASI_MUAT { get; set; }

        }

        public DataListVesselProgres GetDataPermohonanTambatHarIniByDermaga(int draw, int start, int length, string search, string dermaga, string fLine, string cbgKode)
        {
            int count = 0;
            int end = start + length;
            DataListVesselProgres result = new DataListVesselProgres();
            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            IEnumerable<VW_MON_VESPROG_BGS> listData = null;

            string ikatTali = (fLine.ToUpper() == "QUEUE") ? "FIRST_LINE is null" : "FIRST_LINE is not null";


            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                if (string.IsNullOrEmpty(search))
                {
                    string sql = "SELECT KODE_CABANG, ASAL_DATA, NAMA_KAPAL, KODE_KAPAL, LOA, GT_KAPAL, NAMA_AGEN, " +
                        "NAMA_PELABUHAN_ASAL, NAMA_PELABUHAN_TUJUAN, NO_PKK, NO_PPK1, NO_PPK_JASA, PNT_TGL_MULAI, " +
                        "PNT_TGL_SELESAI, PNT_KD_DMG, REA_TGL_MULAI, REA_TGL_SELESAI, REA_KD_DMG, REA_NAMA_DMG, " +
                        "KADE_METER, STATUS, PROGRESS FROM VW_MON_VESPROG_BGS WHERE REA_KD_DMG=:DERMAGA AND KODE_CABANG=:KODE_CABANG order by PROGRESS desc";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<VW_MON_VESPROG_BGS>(fullSql, new { a = start, b = end, DERMAGA = dermaga, KODE_CABANG = cbgKode });
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { DERMAGA = dermaga, KODE_CABANG = cbgKode });
                }
                else
                {
                    string sql = "SELECT KODE_CABANG, ASAL_DATA, NAMA_KAPAL, KODE_KAPAL, LOA, GT_KAPAL, NAMA_AGEN, " +
                        "NAMA_PELABUHAN_ASAL, NAMA_PELABUHAN_TUJUAN, NO_PKK, NO_PPK1, NO_PPK_JASA, PNT_TGL_MULAI, " +
                        "PNT_TGL_SELESAI, PNT_KD_DMG, REA_TGL_MULAI, REA_TGL_SELESAI, REA_KD_DMG, REA_NAMA_DMG, " +
                        "KADE_METER, STATUS, PROGRESS FROM VW_MON_VESPROG_BGS WHERE UPPER(NO_PKK||NAMA_AGEN||NAMA_KAPAL) LIKE '%' || UPPER(:param) || '%' " +
                        "AND REA_KD_DMG=:DERMAGA AND KODE_CABANG=:KODE_CABANG order by PROGRESS desc";
                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<VW_MON_VESPROG_BGS>(fullSql, new { a = start, b = end, param = search, DERMAGA = dermaga, KODE_CABANG = cbgKode });
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { DERMAGA = dermaga, param = search, KODE_CABANG = cbgKode });
                }
            }
            catch (Exception e) {
                listData = new List<VW_MON_VESPROG_BGS>();
            }
            connection.Close();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();

            return result;
        }

        public DataListVesselTambat GetDataPermohonanTambatHarIni(int draw, int start, int length, string search, string fLine, string cbgKode, string mdmgKode)
        {
            int count = 0;
            int end = start + length;
            DataListVesselTambat result = new DataListVesselTambat();
            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            IEnumerable<VW_MON_VESTAMBAT_BGS> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                if (cbgKode == "0")
                {
                    if (string.IsNullOrEmpty(search))
                    {
                        string sql = "SELECT KODE_CABANG, ASAL_DATA, NAMA_KAPAL, KODE_KAPAL, LOA, GT_KAPAL, NAMA_AGEN, " +
                            "NAMA_PELABUHAN_ASAL, NAMA_PELABUHAN_TUJUAN, NO_PKK, NO_PPK1, NO_PPK_JASA, PNT_TGL_MULAI, " +
                            "PNT_TGL_SELESAI, PNT_KD_DMG, REA_TGL_MULAI, REA_TGL_SELESAI, REA_KD_DMG, REA_NAMA_DMG, " +
                            "KADE_METER, STATUS, PROGRESS, COLOR FROM VW_MON_VESTAMBAT_BGS order by PNT_TGL_MULAI desc";

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_MON_VESTAMBAT_BGS>(fullSql, new { a = start, b = end, KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                    }
                    else
                    {
                        string sql = "SELECT KODE_CABANG, ASAL_DATA, NAMA_KAPAL, KODE_KAPAL, LOA, GT_KAPAL, NAMA_AGEN, " +
                            "NAMA_PELABUHAN_ASAL, NAMA_PELABUHAN_TUJUAN, NO_PKK, NO_PPK1, NO_PPK_JASA, PNT_TGL_MULAI, " +
                            "PNT_TGL_SELESAI, PNT_KD_DMG, REA_TGL_MULAI, REA_TGL_SELESAI, REA_KD_DMG, REA_NAMA_DMG, " +
                            "KADE_METER, STATUS, PROGRESS, COLOR FROM VW_MON_VESTAMBAT_BGS WHERE UPPER(NO_PKK||NAMA_AGEN||NAMA_KAPAL) LIKE '%' || UPPER(:param) || '%' " +
                            "order by PNT_TGL_MULAI desc";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_MON_VESTAMBAT_BGS>(fullSql, new { a = start, b = end, param = search, KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { param = search, KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(search))
                    {
                        string sql = "SELECT KODE_CABANG, ASAL_DATA, NAMA_KAPAL, KODE_KAPAL, LOA, GT_KAPAL, NAMA_AGEN, " +
                            "NAMA_PELABUHAN_ASAL, NAMA_PELABUHAN_TUJUAN, NO_PKK, NO_PPK1, NO_PPK_JASA, PNT_TGL_MULAI, " +
                            "PNT_TGL_SELESAI, PNT_KD_DMG, REA_TGL_MULAI, REA_TGL_SELESAI, REA_KD_DMG, REA_NAMA_DMG, " +
                            "KADE_METER, STATUS, PROGRESS, COLOR FROM VW_MON_VESTAMBAT_BGS " +
                            "WHERE REA_KD_DMG=:REA_KD_DMG AND KODE_CABANG=:KD_CABANG order by PNT_TGL_MULAI desc";

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_MON_VESTAMBAT_BGS>(fullSql, new { a = start, b = end, KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                    }
                    else
                    {
                        string sql = "SELECT KODE_CABANG, ASAL_DATA, NAMA_KAPAL, KODE_KAPAL, LOA, GT_KAPAL, NAMA_AGEN, " +
                            "NAMA_PELABUHAN_ASAL, NAMA_PELABUHAN_TUJUAN, NO_PKK, NO_PPK1, NO_PPK_JASA, PNT_TGL_MULAI, " +
                            "PNT_TGL_SELESAI, PNT_KD_DMG, REA_TGL_MULAI, REA_TGL_SELESAI, REA_KD_DMG, REA_NAMA_DMG, " +
                            "KADE_METER, STATUS, PROGRESS, COLOR FROM VW_MON_VESTAMBAT_BGS WHERE UPPER(NO_PKK||NAMA_AGEN||NAMA_KAPAL) LIKE '%' || UPPER(:param) || '%' " +
                            "AND REA_KD_DMG=:REA_KD_DMG AND KODE_CABANG=:KD_CABANG order by PNT_TGL_MULAI desc";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_MON_VESTAMBAT_BGS>(fullSql, new { a = start, b = end, param = search, KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { param = search, KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                    }
                }
            }
            catch (Exception e)
            {
                listData = new List<VW_MON_VESTAMBAT_BGS>();
            }
            connection.Close();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();

            return result;
        }

        public DataListVesselTambat GetDataKeberangkatanKapal(int draw, int start, int length, string search, string fLine, string cbgKode, string mdmgKode)
        {
            int count = 0;
            int end = start + length;
            DataListVesselTambat result = new DataListVesselTambat();
            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            IEnumerable<VW_MON_VESTAMBAT_BGS> listData = null;

            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                if (cbgKode == "0")
                {
                    if (string.IsNullOrEmpty(search))
                    {
                        string sql = "SELECT KODE_CABANG, ASAL_DATA, NAMA_KAPAL, KODE_KAPAL, LOA, GT_KAPAL, NAMA_AGEN, " +
                            "NAMA_PELABUHAN_ASAL, NAMA_PELABUHAN_TUJUAN, NO_PKK, NO_PPK1, NO_PPK_JASA, PNT_TGL_MULAI, " +
                            "PNT_TGL_SELESAI, PNT_KD_DMG, REA_TGL_MULAI, REA_TGL_SELESAI, REA_KD_DMG, REA_NAMA_DMG, " +
                            "KADE_METER, STATUS, PROGRESS, COLOR FROM VW_MON_VESDEPART_BGS order by PNT_TGL_MULAI desc";

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_MON_VESTAMBAT_BGS>(fullSql, new { a = start, b = end, KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                    }
                    else
                    {
                        string sql = "SELECT KODE_CABANG, ASAL_DATA, NAMA_KAPAL, KODE_KAPAL, LOA, GT_KAPAL, NAMA_AGEN, " +
                            "NAMA_PELABUHAN_ASAL, NAMA_PELABUHAN_TUJUAN, NO_PKK, NO_PPK1, NO_PPK_JASA, PNT_TGL_MULAI, " +
                            "PNT_TGL_SELESAI, PNT_KD_DMG, REA_TGL_MULAI, REA_TGL_SELESAI, REA_KD_DMG, REA_NAMA_DMG, " +
                            "KADE_METER, STATUS, PROGRESS, COLOR FROM VW_MON_VESDEPART_BGS WHERE UPPER(NO_PKK||NAMA_AGEN||NAMA_KAPAL) LIKE '%' || UPPER(:param) || '%' " +
                            "order by PNT_TGL_MULAI desc";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_MON_VESTAMBAT_BGS>(fullSql, new { a = start, b = end, param = search, KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { param = search, KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(search))
                    {
                        string sql = "SELECT KODE_CABANG, ASAL_DATA, NAMA_KAPAL, KODE_KAPAL, LOA, GT_KAPAL, NAMA_AGEN, " +
                            "NAMA_PELABUHAN_ASAL, NAMA_PELABUHAN_TUJUAN, NO_PKK, NO_PPK1, NO_PPK_JASA, PNT_TGL_MULAI, " +
                            "PNT_TGL_SELESAI, PNT_KD_DMG, REA_TGL_MULAI, REA_TGL_SELESAI, REA_KD_DMG, REA_NAMA_DMG, " +
                            "KADE_METER, STATUS, PROGRESS, COLOR FROM VW_MON_VESDEPART_BGS " +
                            "WHERE REA_KD_DMG=:REA_KD_DMG AND KODE_CABANG=:KD_CABANG order by PNT_TGL_MULAI desc";

                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_MON_VESTAMBAT_BGS>(fullSql, new { a = start, b = end, KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                    }
                    else
                    {
                        string sql = "SELECT KODE_CABANG, ASAL_DATA, NAMA_KAPAL, KODE_KAPAL, LOA, GT_KAPAL, NAMA_AGEN, " +
                            "NAMA_PELABUHAN_ASAL, NAMA_PELABUHAN_TUJUAN, NO_PKK, NO_PPK1, NO_PPK_JASA, PNT_TGL_MULAI, " +
                            "PNT_TGL_SELESAI, PNT_KD_DMG, REA_TGL_MULAI, REA_TGL_SELESAI, REA_KD_DMG, REA_NAMA_DMG, " +
                            "KADE_METER, STATUS, PROGRESS, COLOR FROM VW_MON_VESDEPART_BGS WHERE UPPER(NO_PKK||NAMA_AGEN||NAMA_KAPAL) LIKE '%' || UPPER(:param) || '%' " +
                            "AND REA_KD_DMG=:REA_KD_DMG AND KODE_CABANG=:KD_CABANG order by PNT_TGL_MULAI desc";
                        fullSql = fullSql.Replace("sql", sql);
                        listData = connection.Query<VW_MON_VESTAMBAT_BGS>(fullSql, new { a = start, b = end, param = search, KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                        fullSqlCount = fullSqlCount.Replace("sql", sql);
                        count = connection.ExecuteScalar<int>(fullSqlCount, new { param = search, KD_CABANG = cbgKode, REA_KD_DMG = mdmgKode });
                    }
                }
            }
            catch (Exception e)
            {
                listData = new List<VW_MON_VESTAMBAT_BGS>();
            }
            connection.Close();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();

            return result;
        }

        public VW_MON_VESPROG_BGS GetDataTambatByKapal(string dermaga, string cbgKode, string Noppk)
        {
            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            VW_MON_VESPROG_BGS listData = null;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                string sql = "SELECT KODE_CABANG, ASAL_DATA, NAMA_KAPAL, KODE_KAPAL, LOA, GT_KAPAL, NAMA_AGEN, " +
                        "NAMA_PELABUHAN_ASAL, NAMA_PELABUHAN_TUJUAN, NO_PKK, NO_PPK1, NO_PPK_JASA, PNT_TGL_MULAI, " +
                        "PNT_TGL_SELESAI, PNT_KD_DMG, REA_TGL_MULAI, REA_TGL_SELESAI, REA_KD_DMG, REA_NAMA_DMG, " +
                        "KADE_METER, STATUS, PROGRESS FROM VW_MON_VESPROG_BGS WHERE REA_KD_DMG=:DERMAGA " +
                        "AND KODE_CABANG=:KODE_CABANG AND NO_PPK1=:NO_PPK1 order by PROGRESS desc";

                listData = connection.Query<VW_MON_VESPROG_BGS>(sql, new { DERMAGA = dermaga, KODE_CABANG = cbgKode, NO_PPK1 = Noppk }).FirstOrDefault();
            }
            catch (Exception e)
            {
                listData = null;
            }
            connection.Close();

            return listData;
        }

        public DataListVesselProgres GetDataPermohonanAntrianTambatByDermaga(int draw, int start, int length, string search, string dermaga, string fLine, string cbgKode)
        {
            int count = 0;
            int end = start + length;
            DataListVesselProgres result = new DataListVesselProgres();
            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            IEnumerable<VW_MON_VESPROG_BGS> listData = null;

            string ikatTali = (fLine.ToUpper() == "QUEUE") ? "FIRST_LINE is null" : "FIRST_LINE is not null";


            string fullSql = "SELECT * FROM (SELECT q.*, ROWNUM rnum FROM (sql) q WHERE ROWNUM <= :b) WHERE rnum > :a";
            string fullSqlCount = "SELECT count(*) FROM (sql)";
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();
            try
            {
                if (string.IsNullOrEmpty(search))
                {
                    string sql = "SELECT KODE_CABANG, ASAL_DATA, NAMA_KAPAL, KODE_KAPAL, LOA, GT_KAPAL, NAMA_AGEN, " +
                        "NAMA_PELABUHAN_ASAL, NAMA_PELABUHAN_TUJUAN, NO_PKK, NO_PPK1, NO_PPK_JASA, PNT_TGL_MULAI, " +
                        "PNT_TGL_SELESAI, PNT_KD_DMG, REA_TGL_MULAI, REA_TGL_SELESAI, REA_KD_DMG, REA_NAMA_DMG, " +
                        "KADE_METER, STATUS, PROGRESS FROM VW_MON_VESQUE_BGS WHERE PNT_KD_DMG=:DERMAGA AND KODE_CABANG=:KODE_CABANG order by progress desc";

                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<VW_MON_VESPROG_BGS>(fullSql, new { a = start, b = end, DERMAGA = dermaga, KODE_CABANG = cbgKode });
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { DERMAGA = dermaga, KODE_CABANG = cbgKode });
                }
                else
                {
                    string sql = "SELECT KODE_CABANG, ASAL_DATA, NAMA_KAPAL, KODE_KAPAL, LOA, GT_KAPAL, NAMA_AGEN, " +
                        "NAMA_PELABUHAN_ASAL, NAMA_PELABUHAN_TUJUAN, NO_PKK, NO_PPK1, NO_PPK_JASA, PNT_TGL_MULAI, " +
                        "PNT_TGL_SELESAI, PNT_KD_DMG, REA_TGL_MULAI, REA_TGL_SELESAI, REA_KD_DMG, REA_NAMA_DMG, " +
                        "KADE_METER, STATUS, PROGRESS FROM VW_MON_VESQUE_BGS WHERE UPPER(NO_PKK||NAMA_AGEN||NAMA_KAPAL) LIKE '%' || UPPER(:param) || '%' " +
                        "AND PNT_KD_DMG=:DERMAGA AND KODE_CABANG=:KODE_CABANG order by progress desc";
                    fullSql = fullSql.Replace("sql", sql);
                    listData = connection.Query<VW_MON_VESPROG_BGS>(fullSql, new { a = start, b = end, param = search, DERMAGA = dermaga, KODE_CABANG = cbgKode });
                    fullSqlCount = fullSqlCount.Replace("sql", sql);
                    count = connection.ExecuteScalar<int>(fullSqlCount, new { DERMAGA = dermaga, param = search, KODE_CABANG = cbgKode });
                }
            }
            catch (Exception e)
            {
                listData = new List<VW_MON_VESPROG_BGS>();
            }
            connection.Close();
            result.draw = draw;
            result.recordsFiltered = count;
            result.recordsTotal = count;
            result.data = listData.ToList();

            return result;
        }

        public List<VW_COUNTER_PENETAPAN_TAMBAT> GetCounterPmhTambatByDermaga() {
            List<VW_COUNTER_PENETAPAN_TAMBAT> result = new List<VW_COUNTER_PENETAPAN_TAMBAT>();
            
            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = "SELECT KD_CABANG, NAMA_CABANG, KODE_ASAL_REA REA_KD_DMG, NAMA_ASAL_REA REA_NAMA_DMG, JML, KELUAR, MASUK FROM VW_MON_TAMBAT_COUNT_BGS";
                result = connection.Query<VW_COUNTER_PENETAPAN_TAMBAT>(sql).ToList();
                
            }
            catch (Exception e) {
                string ex = e.Message.ToString();
                    }
            connection.Close();
            return result;
        }

        public DataDataDetailsKegiatanKapal getDataDEtailsPmhTambat(string tppkb1Nomor)
        {
            DataDataDetailsKegiatanKapal result = new DataDataDetailsKegiatanKapal();
            IDbConnection connection = ServiceConfig.DefaultDbConnection;
            if (connection.State.Equals(ConnectionState.Closed))
                connection.Open();

            try
            {
                string sql = "select temps.tppkb1_nomor, temps.tppkb1_no_form, temps.tppkb1_nama_kapal, temps.agen, to_char(pnt_mulai,'dd/mm/yyyy hh24:mi:ss')pnt_mulai, to_char(pnt_selesai,'dd/mm/yyyy hh24:mi:ss')pnt_selesai, to_char(FIRST_LINE,'dd/mm/yyyy hh24:mi:ss')FIRST_LINE, dermaga, rec_stat, " +
                                "	case  " +
                                "	when FIRST_LINE is null then 0 " +
                                "	when FIRST_LINE is not null then 		 " +
                                "		round((round((sysdate - FIRST_LINE) * (60 * 24))/ " +
                                "    round((pnt_selesai - FIRST_LINE) * (60 * 24)))*100) " +
                                "		end as progres " +
                                "from ( " +
                                "    select " +
                                "    a.tppkb1_nomor, " +
                                "    a.tppkb1_no_form, " +
                                "    a.tppkb1_nama_kapal,  " +
                                "          (select a.mplg_kode||' - '||mplg_nama from safm_pelanggan@mdm_prod where mplg_kode = a.mplg_kode and kd_cabang ='2')agen, " +
                                "    min(b.pntmb_jammtambat)pnt_mulai, max(b.pntmb_jamstambat)pnt_selesai, " +
                                "    (select min(BKTBT_JAMMTAMBAT) from upkt_bukti_tambat where tppkb1_nomor = a.tppkb1_nomor and rec_stat in ('B','P'))FIRST_LINE, " +
                                "    (select mdmg_kode||' - '||mdmg_nama from upkm_dermaga where mdmg_kode = b.mdmg_kode)dermaga, " +
                                "    b.rec_stat " +
                                "    from upkt_pmh_ppkb1 a,upkt_pnt_tambat b  " +
                                "    where " +
                                "    a.tppkb1_nomor = b.tppkb1_nomor " +
                                "    and b.rec_stat in ('B','P') " +
                                "    and a.tppkb1_nomor = :PARAMS " +
                                "    and to_date(to_char(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy') between to_date(to_char(b.pntmb_tglmtambat-1,'dd/mm/yyyy'),'dd/mm/yyyy') " +
                                "    and to_date(to_char(b.pntmb_tglstambat,'dd/mm/yyyy'),'dd/mm/yyyy')  " +
                                "    group by a.tppkb1_nomor,a.tppkb1_no_form,a.tppkb1_nama_kapal,b.mdmg_kode,b.rec_stat,a.mplg_kode  " +
                                "    order by 6 " +
                                ")temps";
                result = connection.Query<DataDataDetailsKegiatanKapal>(sql, new { PARAMS = tppkb1Nomor }).SingleOrDefault();

            }
            catch (Exception e)
            {
                string ex = e.Message.ToString();
            }
            connection.Close();
            return result;
        }

        
    }
}