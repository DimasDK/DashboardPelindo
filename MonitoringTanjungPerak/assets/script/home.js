﻿var Dashboard = function (){ 
    
    var a = function (a, b, c) {
        a.push(Math.floor(Math.random() * (c - b + 1)) + b), a.shift(), g.update();
    }

    var getRandomColor = function (x) {
        var arryclr = ["primary", "danger", "success", "warning", "info", "red", "pink", "purple", "indigo", "blue", "cyan", "teal", "green", "lime", "yellow", "amber", "orange", "deep-orange", "brown", "grey"];
        var xd = (Math.floor(Math.random() * (20 - 0)) + 0);

        if (x != null) {
            return arryclr[x];
        }
        return arryclr[xd];
    }

    var getDataGrKCounter = function(){
        $.ajax({
            url: "/Home/CounterAllPmhGrkKapal",
            method: "GET",
            dataType: "json",
            success: function (msg) {
                $("[id=cnGkms]").html(0);
                if (msg.length > 0) {
                    $.each(msg,function(e, v){
                        if(v.AREACODE == "IDSUB" && v.GERAKAN == "MASUK") {
                            $("[id=cnGkms]").html(v.COUNTER_JASA);
                        }

                        if(v.AREACODE == "IDSUB" && v.GERAKAN == "PINDAH") {
                            $("[id=cnGkps]").html(v.COUNTER_JASA);
                        }

                        if(v.AREACODE == "IDSUB" && v.GERAKAN == "KELUAR") {
                            $("[id=cnGkks]").html(v.COUNTER_JASA);
                        }

                        if(v.AREACODE == "IDGRE" && v.GERAKAN == "MASUK") {
                            $("[id=cnGkmg]").html(v.COUNTER_JASA);
                        }

                        if(v.AREACODE == "IDGRE" && v.GERAKAN == "PINDAH") {
                            $("[id=cnGkpg]").html(v.COUNTER_JASA);
                        }

                        if(v.AREACODE == "IDGRE" && v.GERAKAN == "KELUAR") {
                            $("[id=cnGkkg]").html(v.COUNTER_JASA);
                        }
                    });                            
                }
            }
        });
    }
    
    var getDataCounter = function () {
        $.ajax({
            url: "/Home/CounterAllPmhTambat",
            method: "GET",
            dataType: "json",
            success: function (msg) {
                if (msg.length > 0) {
                    var nm = 0, total = 0;
                    $.each(msg, function (ec, vc) {
                        total = total + parseFloat(vc.COUNTER_TODAY);
                    });

                    var apn = "", rmx = 0;
                    $.each(msg, function (e, v) {
                        nm = (nm > 20) ? 0 : nm;
                        var clr = getRandomColor(nm);
                        var nmdmg = v.MDMG_NAMA.split(' - ');
                        apn += "<div class='col-xl-3 col-lg-6 col-sm-12 border-right-blue-grey border-right-lighten-5' style='min-height:140px;'>" +
                                    "	<div class='media px-1'>" +
                                    "		<div class='cursor-pointer media-left media-middle' alt=" + v.MDMG_KODE + " data-nama='" + nmdmg[1] + "'>" +
                                    "			<i class='icon-ship font-large-1 blue-grey'></i>" +
                                    "		</div>" +
                                    "		<div class='cursor-pointer media-body text-xs-right' alt=" + v.MDMG_KODE + " data-nama='" + nmdmg[1] + "'>" +
                                    "			<span class='font-large-2 text-bold-300 " + clr + "'>" + v.COUNTER_TODAY + "</span>" +
                                    "		</div>" +
                                    "		<div class='cursor-pointer text-muted pb-1' alt=" + v.MDMG_KODE + " data-nama='" + nmdmg[1] + "' style='min-height:54px'><b style='font-size:10px'>" + v.MDMG_NAMA + "</b></div>" +
                                    "		<progress class='progress progress-sm progress-" + clr + "' value='100' max='100'></progress>" +
                                    "	</div>" +
                                    "</div>";

                        nm++;
                    });

                    $("div[id=counterPmhDermaga]").html(apn);
                    $('.cursor-pointer').click(function () {
                        var namadmg = $(this).data('nama');
                        window.location.href = '@Url.Action("PmhTambatHrIniByKapal", "Home")?mdmg=' + $(this).attr('alt') + '&nm=' + namadmg;
                    });
                }
            }
        }).done(function(){
            getDataGrKCounter();
        });
    }

    var autoreloadsss = function() {
        setInterval(function(){ getDataCounter() }, 1000*60*(setCounterVpReload));}  
    
    var pageEvent = function() {
        $('a.reloadVesselProgressCounter').click(function () {
            getDataCounter();
        });

        $('a.reloadGerakanKapalCounter').click(function () {
            getDataGrKCounter();
        });

        $("[id=btnShowdrdn]").click(function(e) {
            e.preventDefault();
            $('[id=areaSetReload]').removeClass("hide");
        });

        $("[id=btnShowdrdnGr]").click(function(e) {
            e.preventDefault();
            $('[id=areaGrSetReload]').removeClass("hide");
        });

        $('[id=areaSetReload]').focusout(function(){
            $(this).addClass("hide");
        });

        $('[id=areaGrSetReload]').focusout(function(){
            $(this).addClass("hide");
        });

        $("select[id=drdArVsPr]").change(function(){
            var vld = $(this).val();
            $.ajax({
                url: "/Home/SetCookiesReload",
                method: "GET",
                dataType: "json",
                data: { reloadParamValue: vld, reloadParamName :"homeIndexCounterVesselProgress" }
            }).done(function () {
                setCounterVpReload = parseInt(vld);
                clearInterval(autoreloadsss);
                autoreloadsss = setInterval(function(){ getDataCounter() }, 1000*60*(setCounterVpReload));
            });
        });

        $("select[id=drdArVsGr]").change(function(){
            var vld = $(this).val();
            $.ajax({
                url: "/Home/SetCookiesReload",
                method: "GET",
                dataType: "json",
                data: { reloadParamValue: vld, reloadParamName :"homeIndexCounterGerakanKapal" }
            }).done(function () {
                setCounterVpReload = parseInt(vld);
                clearInterval(autoreloadsss);
                autoreloadsss = setInterval(function(){ getDataGrKCounter() }, 1000*60*(setCounterVpReload));
            });
        });
    }

    return {
        init: function () {
            getDataCounter();
            initCharts();
            handleModals();
            btnSelesai();
            pageEvent();
        }
    }
}();

App.isAngularJsApp() === !1 && jQuery(document).ready(function () {
    Dashboard.init();
});