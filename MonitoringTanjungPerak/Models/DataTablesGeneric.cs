﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MonitoringTanjungPerak.ViewModels;

namespace MonitoringTanjungPerak.Models
{
    public class DataTablesGeneric<T>
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<T> data { get; set; }
        public List<ReportHeaderDefModel> columns { get; set; }
    }


}