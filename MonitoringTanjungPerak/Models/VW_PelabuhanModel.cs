﻿namespace MonitoringTanjungPerak.ViewHelper
{
    public class VW_PelabuhanModel
    {
        public string id { get; set; }
        public string MPLB_KODE { get; set; }
        public string MPLB_NAMA { get; set; }
        public string MPLB_KOTA { get; set; }
        public string MNEG_KODE { get; set; }
    }
}