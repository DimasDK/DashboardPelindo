﻿using MonitoringTanjungPerak.Entities;
using System.Collections.Generic;
namespace MonitoringTanjungPerak.Models
{
    public class BalanceKapalModel
    {
        public VW_MON_VESPROG_BGS dataKapal { get; set; }
        public IEnumerable<VW_MON_BALANCE_BM> dataBalance { get; set; }
    }
}